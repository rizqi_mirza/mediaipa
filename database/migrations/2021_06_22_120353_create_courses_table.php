<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id('id_courses',11);
            $table->bigInteger('id_categoryCourses')->unsigned();
            $table->foreign('id_categoryCourses')->references('id_category_courses')->on('category_courses');
            $table->string('title_courses');
            $table->string('video_courses');
            $table->string('thumbnail_courses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
