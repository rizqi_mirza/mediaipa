<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResultFormatifTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result_formatif', function (Blueprint $table) {
            $table->id('id_result_formatif',11);
            $table->bigInteger('quizformatif_ID')->unsigned();
            $table->foreign('quizFormatif_ID')->references('id_quiz_formatif')->on('quiz_formatif');
            $table->string('name_students');
            $table->integer('point_formatif');
            $table->integer('correct_formatif');
            $table->integer('wrong_formatif');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('result_formatif');
    }
}
