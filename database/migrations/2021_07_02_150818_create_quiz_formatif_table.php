<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuizFormatifTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quiz_formatif', function (Blueprint $table) {
            $table->id('id_quiz_formatif',11);
            $table->bigInteger('courseID')->unsigned();
            $table->foreign('courseID')->references('id_courses')->on('courses');
            $table->string('name_quiz_formatif');
            $table->string('description_quiz_formatif')->nullable();
            $table->string('image_quiz_formatif')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quiz_formatif');
    }
}
