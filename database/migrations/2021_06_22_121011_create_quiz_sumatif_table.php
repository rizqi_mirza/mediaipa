<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuizSumatifTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quiz_sumatif', function (Blueprint $table) {
            $table->id('id_quiz_sumatif',11);
            $table->bigInteger('id_categoryCourse')->unsigned();
            $table->foreign('id_categoryCourse')->references('id_category_courses')->on('category_courses');
            $table->string('name_quiz_sumatif');
            $table->string('description_quiz_sumatif');
            $table->string('image_quiz_sumatif')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quiz_sumatif');
    }
}
