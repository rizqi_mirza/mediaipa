<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionQuizFormatifTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_quiz_formatif', function (Blueprint $table) {
            $table->id('id_question_quiz_formatif',11);
            $table->bigInteger('id_quizFormatif')->unsigned();
            $table->foreign('id_quizFormatif')->references('id_quiz_formatif')->on('quiz_formatif');
            $table->string('question_quiz_formatif');
            $table->string('image_question_formatif')->nullable();
            // $table->string('video_question_formatif')->nullable();
            $table->string('voice_question_formatif')->nullable();
            $table->string('correct_answer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_quiz_formatif');
    }
}
