<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionQuizSumatifTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_quiz_sumatif', function (Blueprint $table) {
            $table->id('id_question_quiz_sumatif',11);
            $table->bigInteger('id_quizSumatif')->unsigned();
            $table->foreign('id_quizSumatif')->references('id_quiz_sumatif')->on('quiz_sumatif');
            $table->string('question_quiz_sumatif');
            $table->string('image_question_sumatif')->nullable();
            // $table->string('video_question_sumatif')->nullable();
            $table->string('voice_question_sumatif')->nullable();
            $table->string('answer1');
            $table->string('answer2');
            $table->string('answer3');
            $table->enum('correct_answer', ['answer1', 'answer2', 'answer3']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_quiz_sumatif');
    }
}
