<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResultSumatifTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result_sumatif', function (Blueprint $table) {
            $table->id('id_result_sumatif',11);
            $table->bigInteger('quizsumatif_ID')->unsigned();
            $table->foreign('quizsumatif_ID')->references('id_quiz_sumatif')->on('quiz_sumatif');
            $table->string('name_students');
            $table->integer('point_sumatif');
            $table->integer('correct_sumatif');
            $table->integer('wrong_sumatif');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('result_sumatif');
    }
}
