<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\QuizFormatif;
use App\Models\QuestionQuizFormatif;
use App\Models\AnswerFormatif;
use App\Models\ResultFormatif;
use App\Models\Courses;
use App\Models\CategoryCourses;
use Session;

class playQuizFormatifController extends Controller
{
    public function quizFormatif(){
        $siswa = Session::get('nisSiswa');
        $quiz = QuizFormatif::all();
        return view('user.quiz_formatif', compact('siswa','quiz'));
    }

    public function detailQuizFormatif($id){
        $siswa = Session::get('nisSiswa');
        session()->put('Score', 0);
        return view('user2.play_quiz_formatif', compact('siswa','id'));
    }

    public function playQuiz($id){
        $quiz = QuestionQuizFormatif::where('id_quiz_formatif',$id)->join('quiz_formatif',
        'question_quiz_formatif.id_quizFormatif','quiz_formatif.id_quiz_formatif')->get();


        return $quiz;
    }

    public function checkAnswer(Request $request,$id){
        $siswa = Session::get('nisSiswa');
        $question = QuestionQuizFormatif::where('id_question_quiz_formatif',$request->id)->first();

        if($question['correct_answer'] == $request->answer){
            $correct = session()->get('Score') + 1;
            session()->put('Score', $correct);
            return [
                "answer" => true
            ];
        }
        return [
             "answer" => false
        ];

        // return $check;
    }

    public function result($id){
        $correct = 0;
        // $quizID = QuizFormatif::where('id_quiz_formatif',$id)->first();
        $correct = session()->get('Score');
        session()->put('Score',0);
        $siswa = Session::get('nisSiswa');
        $quiz = QuestionQuizFormatif::where('id_quiz_formatif',$id)->join('quiz_formatif',
        'question_quiz_formatif.id_quizFormatif','quiz_formatif.id_quiz_formatif')->get();

        $point = round((100 / count($quiz))*$correct);
        $wrong = count($quiz) - $correct;

        $result = new ResultFormatif;
        $result->quizformatif_ID = $id;
        $result->name_students = $siswa;
        $result->point_formatif = $point;
        $result->correct_formatif = $correct;
        $result->wrong_formatif = $wrong;
        $payload = $result->save();
        return view('user2.result_formatif',compact('siswa','correct','point','wrong'));
    }

    public function resultFormatif(Request $request,$id){
        $siswa = Session::get('nisSiswa');
        $quizID = QuizFormatif::where('id_quiz_formatif',$id)->first();
        $quiz = QuestionQuizFormatif::where('id_quiz_formatif',$id)->join('quiz_formatif',
        'question_quiz_formatif.id_quizFormatif','quiz_formatif.id_quiz_formatif')->get();
        $correct = 0;

        foreach($quiz as $question){
            if($question->correct_answer == $request->post($question->id_question_quiz_formatif)){
                $correct+=1;
            }
        }

        $point = round((100 / count($quiz))*$correct);
        $wrong = count($quiz) - $correct;

        $result = new ResultFormatif;
        $result->quizformatif_ID = $id;
        $result->name_students = $siswa;
        $result->point_formatif = $point;
        $result->correct_formatif = $correct;
        $result->wrong_formatif = $wrong;
        $payload = $result->save();

        return view('user.result_formatif',compact('siswa','quizID','point','correct','wrong'));
    }
}
