<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Middleware\PlayQuiz;
use Illuminate\Http\Request;
use App\Models\QuizSumatif;
use App\Models\QuestionQuizSumatif;
use App\Models\ResultSumatif;
use Session;

class PlayQuizSumatifController extends Controller
{
    //
    public function quizSumatif(){
        $siswa = Session::get('nisSiswa');
        $quiz = QuizSumatif::simplePaginate(6);
        return view('user2.quiz_sumatif', compact('siswa','quiz'));
    }

    public function detailQuizSumatif($id){
        $siswa = Session::get('nisSiswa');
        $quiz =  QuestionQuizSumatif::where('id_quiz_sumatif',$id)->join('quiz_sumatif',
        'question_quiz_sumatif.id_quizSumatif','quiz_sumatif.id_quiz_sumatif')->get();
        // $quiz = QuizSumatif::where('id_quiz_sumatif',$id)->join('question_quiz_sumatif','quiz_sumatif.id_quiz_sumatif',
        // 'question_quiz_sumatif.id_quizSumatif')->first();

        // $quiz = QuizSumatif::where($id)->with('questions')->first();
        // dd($quiz);
        return view('user2.play_quiz_sumatif', compact('quiz','siswa'));
    }

    public function result(Request $request,$id){
        $siswa = Session::get('nisSiswa');
        $quizID = QuizSumatif::where('id_quiz_sumatif',$id)->first();
        $quiz =  QuestionQuizSumatif::where('id_quiz_sumatif',$id)->join('quiz_sumatif',
        'question_quiz_sumatif.id_quizSumatif','quiz_sumatif.id_quiz_sumatif')->get();
        $correct = 0;

        foreach($quiz as $question){
            if($question->correct_answer === $request->post($question->id_question_quiz_sumatif)){
                $correct+=1;
            }
        }

        $point = round((100 / count($quiz))*$correct);
        $wrong = count($quiz) - $correct;

        $result = new ResultSumatif;
        $result->quizSumatif_ID = $id;
        $result->name_students = $siswa;
        $result->point_sumatif = $point;
        $result->correct_sumatif = $correct;
        $result->wrong_sumatif = $wrong;
        $payload = $result->save();


        // dd($payload);
        //return redirect

        return view('user2.result_sumatif', compact('siswa','quizID','point','correct','wrong'));

    }
}
