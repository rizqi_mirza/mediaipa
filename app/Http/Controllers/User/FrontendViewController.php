<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CategoryCourses;
use App\Models\Courses;
use App\Models\QuizFormatif;
use Session;

class FrontendViewController extends Controller
{
    //

    public function index(){
        return view('user2.index');
    }

    public function instruction(){
        return view('user2.instruction');
    }

    public function subCourse(){
        $siswa = Session::get('nisSiswa');
        $subCourse = CategoryCourses::simplePaginate(4);
        // $quiz = QuizSumatif::simplePaginate(6);
        return view('user2.sub_course', compact('subCourse','siswa'));
    }

    public function course($title){
        $siswa = Session::get('nisSiswa');
        $course = Courses::where('name_category_courses',$title)->join('category_courses','courses.id_categoryCourses','=','category_courses.id_category_courses')->simplePaginate(4);
        return view('user2.course', compact('course','siswa'));
    }

    public function detailCourse($title){
        $siswa = Session::get('nisSiswa');
        $detailCourse = Courses::where('title_courses',$title)->join('category_courses','courses.id_categoryCourses','=','category_courses.id_category_courses')->first();
        $quiz = QuizFormatif::where('title_courses',$title)->join('courses','quiz_formatif.courseID','courses.id_courses')->first();
        return view('user2.detail-course', compact('detailCourse','quiz','siswa'));
    }

    public function about(){
        return view('user2.about');
    }

    public function competence(){
        return view('user2.competence');
    }

    // public function loginQuiz(){
    //     if(session()->get('nisSiswa')){
    //         return redirect('/quiz-play');
    //     }
    //     return view('user.login-quiz');
    // }

    public function startStudy(){
        if(session()->get('nisSiswa')){
            return redirect('/study-play');
        }
        return view('user2.start_study');
    }
}
