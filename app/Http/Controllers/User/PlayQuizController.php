<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Courses;
use App\Models\CategoryCourses;
use Session;

class PlayQuizController extends Controller
{
    //
    public function loginKuis(Request $request){
        $nis = $request->nama_siswa;
        if($nis){
            Session::put('nisSiswa',$nis);

            return redirect('/quiz-play');
        }

        return redirect('/login-quiz');
    }

    public function pageKuis(){
        $siswa = Session::get('nisSiswa');
        return view('user.quiz', compact('siswa'));
    }

    public function logout(){
        Session::flush();
        return redirect('/login-quiz');
    }

    public function quizSumatif(){
        $siswa = Session::get('nisSiswa');
        return view('user.quiz_sumatif', compact('siswa'));
    }


}
