<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;

class StartStudyController extends Controller
{
    public function loginStudy(Request $request){
        $nis = $request->nama_siswa;
        if($nis){
            Session::put('nisSiswa',$nis);

            return redirect('/study-play');
        }

        return redirect('/start-study');
    }

    public function pageStudy(){
        $siswa = Session::get('nisSiswa');
        return view('user2.play_study', compact('siswa'));
    }

    public function logout(){
        Session::flush();
        return redirect('/start-study');
    }
}
