<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\QuizSumatif;
use App\Models\QuestionQuizSumatif;
use Session;


class QuestionSumatifController extends Controller
{
    public function post(Request $request,$title){
        $saveId = QuizSumatif::where('name_quiz_sumatif',$title)->first();

        $this->validate($request,[
            "question_quiz" => "required",
            "answerA" => "required",
            "answerB" => "required",
            "answerC" => "required",
            "correct_answer" => "required",
            "image_question" => 'image|mimes:jpeg,png,jpg|max:2048|nullable',
            "voice_question" => 'nullable|mimes:audio/mpeg,mpga,mp3,wav,aac'
            // "video_question" => 'mimes:mp4|nullable'
        ]);

        $image = $request->file('image_question');
        if($image != null){
            $name_image = time().'_'.$image->getClientOriginalName();
            $destination = 'files/quiz-sumatif/question';
            $image->move($destination,$name_image);
        }else{
            $name_image = "null";
        }

        $voice = $request->file('voice_question');
        if($voice != null){
            $name_voice = time().'_'.$voice->getClientOriginalName();
            $destinationvoice = 'files/quiz-sumatif/question/voice';
            $voice->move($destinationvoice,$name_voice);
        }else{
            $name_voice = "null";
        }

        // $video = $request->file('video_question');
        // if($video != null){
        //     $name_video = time().'_'.$video->getClientOriginalName();
        //     $destinationvideo = 'files/quiz-sumatif/question/video';
        //     $video->move($destinationvideo,$name_video);
        // }else{
        //     $name_video = "null";
        // }


        $question = new QuestionQuizSumatif;
        $question->id_quizSumatif = $saveId->id_quiz_sumatif;
        $question->question_quiz_sumatif = $request->question_quiz;
        $question->image_question_sumatif = $name_image;
        // $question->video_question_sumatif = $name_video;
        $question->voice_question_sumatif = $name_voice;
        $question->answer1 = $request->answerA;
        $question->answer2 = $request->answerB;
        $question->answer3 = $request->answerC;
        $question->correct_answer = $request->correct_answer;
        $payload = $question->save();

        // dd($payload);
        return redirect('/questionSumatif/'.$title);

    }

    public function edit($titleQuestion,$title){
        $name= Session::get('name');
        $question = QuestionQuizSumatif::where('id_question_quiz_sumatif',$titleQuestion)->first();
        return view('admin.quiz-sumatif.questionQuiz.edit_question',compact('title','question','name'));
    }

    public function update(Request $request,$title){
        $saveId = QuizSumatif::where('name_quiz_sumatif',$title)->first();
        $this->validate($request,[
            "question_quiz" => "required",
            "answerA" => "required",
            "answerB" => "required",
            "answerC" => "required",
            "correct_answer" => "required",
            "image_question" => 'image|mimes:jpeg,png,jpg|max:2048|nullable',
            "voice_question" => 'nullable|mimes:audio/mpeg,mpga,mp3,wav,aac'
            // "video_question" => 'mimes:mp4|nullable'
        ]);
        $id = $request->id_question_sumatif;
        $imageOld = $request->imageOld;
        $image = $request->file('image_question');

        if($image != ''){
            $image = $request->file('image_question');
            $name_image = time().'_'.$image->getClientOriginalName();
            $destination = 'files/quiz-sumatif/question';
            $image->move($destination,$name_image);
            if(file_exists(public_path('files/quiz-sumatif/question/'.$imageOld))){
                unlink(public_path('files/quiz-sumatif/question/'.$imageOld));
            }
        }else{
            $name_image = $imageOld;
        }
        $voiceOld = $request->voiceOld;
        $voice = $request->file('voice_question');
        if($voice != ''){
            $name_voice = time().'_'.$voice->getClientOriginalName();
            $destinationvoice = 'files/quiz-sumatif/question/voice';
            $voice->move($destinationvoice,$name_voice);
            if(file_exists(public_path('files/quiz-sumatif/question/voice/'.$voiceOld))){
                unlink(public_path('files/quiz-sumatif/question/voice/'.$voiceOld));
            }
        }else{
            $name_voice = $voiceOld;
        }

        // $videoOld = $request->videoOld;
        // $video = $request->file('video_question');
        // if($video != ''){
        //     $name_video = time().'_'.$video->getClientOriginalName();
        //     $destinationvideo = 'files/quiz-sumatif/question/video';
        //     $video->move($destinationvideo,$name_video);
        //     if(file_exists(public_path('files/quiz-sumatif/question/video'.$videoOld))){
        //         unlink(public_path('files/quiz-sumatif/question/video'.$videoOld));
        //     }
        // }else{
        //     $name_video = $videoOld;
        // }
        $data = array(
            "id_question_quiz_sumatif" => $request->id_question_sumatif,
            "id_quizSumatif" => $saveId->id_quiz_sumatif,
            "question_quiz_sumatif" => $request->question_quiz,
            "image_question_sumatif" => $name_image,
            // "video_question_sumatif" => $name_video,
            "voice_question_sumatif" => $name_voice,
            "answer1" => $request->answerA,
            "answer2" => $request->answerB,
            "answer3" => $request->answerC,
            "correct_answer" => $request->correct_answer,
        );

        QuestionQuizSumatif::where('id_question_quiz_sumatif',$id)->update($data);
        return redirect('/questionSumatif/'.$title);

    }

    public function delete($id,$title){
        $question =  QuestionQuizSumatif::findOrFail($id);

        $path = $question->image_question_sumatif;
        if($path !='' || $path !='null'){
            if(file_exists(public_path('files/quiz-sumatif/question/'.$path))){
                unlink(public_path('files/quiz-sumatif/question/'.$path));
            }
        }

        $audio = $question->voice_question_sumatif;
        if($audio !='' || $audio !='null'){
            if(file_exists(public_path('files/quiz-sumatif/question/voice/'.$audio))){
                unlink(public_path('files/quiz-sumatif/question/voice/'.$audio));
            }
        }

        QuestionQuizSumatif::where('id_question_quiz_sumatif',$id)->delete();
        return redirect('/questionSumatif/'.$title);

    }

    public function detail($id,$title){
        $name= Session::get('name');
        $question = QuestionQuizSumatif::where('id_question_quiz_sumatif',$id)->first();
        return view('admin.quiz-sumatif.questionQuiz.detail_question',compact('title','question','name'));
    }

}
