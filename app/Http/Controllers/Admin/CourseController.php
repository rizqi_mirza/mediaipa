<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CategoryCourses;
use App\Models\Courses;
use Session;

class CourseController extends Controller
{

    public function post(Request $request){
        $this->validate($request,[
            "title_course" => 'required',
            "categorycourse" => 'required',
            "thumbnail_course" => 'image|mimes:jpeg,png,jpg|max:2048|nullable',
            "video_course" => 'mimes:mp4|required'
        ]);

        $thumbnail =  $request->file('thumbnail_course');
        if($thumbnail !=''){
            $thumbnailname = time()."_".$thumbnail->getClientOriginalName();
            $destinationthumbnail = 'files/course/thumbnail';
            $thumbnail->move($destinationthumbnail,$thumbnailname);
        }else{
            $thumbnailname = "null";
        }


        $video =  $request->file('video_course');
        if($video !=''){
            $videoname =  time()."_".$video->getClientOriginalName();
            $destination = 'files/course';
            $video->move($destination,$videoname);
        }else{
            $videoname = "null";
        }


        $course = new Courses;
        $course->id_categoryCourses = $request->categorycourse;
        $course->title_courses = $request->title_course;
        $course->video_courses = $videoname;
        $course->thumbnail_courses = $thumbnailname;
        $payload =  $course->save();

        if($payload){
            $request->session()->flash('status','Materi Berhasil Ditambahkan');
            return redirect('/course/course');
        }

    }

    public function detailCourse($title){
        $name= Session::get('name');
        $course = Courses::where('title_courses',$title)->join('category_courses','courses.id_categoryCourses','=','category_courses.id_category_courses')->first();
        return view('admin.course.detail-course', compact('name','course'));
    }

    public function edit($title){
        $name= Session::get('name');
        $course = Courses::where('title_courses',$title)->first();
        $items = CategoryCourses::all(['id_category_courses','name_category_courses']);
        return view('admin.course.edit-course', compact('course','items','name'));
    }

    public function update(Request $request,$title){
        // dd($request);
        $id = $request->id_course;
        $this->validate($request,[
            "title_course" => 'required',
            "categorycourse" => 'required',
            "thumbnail_course" => 'image|mimes:jpeg,png,jpg|max:2048|nullable',
            "video_course" => 'mimes:mp4|nullable'
        ]);

        $videoOld = $request->video_course_old;
        $videoNew = $request->file('video_course');
        if($videoNew != ''){
            $videonew = $request->file('video_course');
            $videoname = time().'_'.$videonew->getClientOriginalName();
            $destination = 'files/course';
            $videonew->move($destination,$videoname);
            if(file_exists(public_path('files/course/'.$videoOld))){
                unlink(public_path('files/course/'.$videoOld));
            }
        }else{
            $videoname = $videoOld;
        }

        $thumbnailOld = $request->thumbnail_old;
        $thumbnail =  $request->file('thumbnail_course');
        if($thumbnail != ''){
            $thumbnailname = time()."_".$thumbnail->getClientOriginalName();
            $destinationthumbnail = 'files/course/thumbnail';
            $thumbnail->move($destinationthumbnail,$thumbnailname);
            if(file_exists(public_path('files/course/thumbnail/'.$thumbnailOld))){
                unlink(public_path('files/course/thumbnail/'.$thumbnailOld));
            }
        }else{
            $thumbnailname = $thumbnailOld;
        }

        $formdata = array(
            'id_categoryCourses' => $request->categorycourse,
            'title_courses' => $request->title_course,
            'video_courses' => $videoname,
            'thumbnail_courses' => $thumbnailname
        );
        // dd($formdata);
        Courses::where('id_courses',$id)->update($formdata);
        return redirect('/course/course');
    }

    public function delete($id){
        $course =  Courses::findOrFail($id);

        $path = $course->video_courses;
        if($path !='' || $path !='null'){
            if(file_exists(public_path('files/course/'.$path))){
                unlink(public_path('files/course/'.$path));
            }
        }

        $thumbnail = $course->thumbnail_courses;
        if($thumbnail !="" || $thumbnail = 'null'){
            if(file_exists(public_path('files/course/thumbnail/'.$thumbnail))){
                unlink(public_path('files/course/thumbnail/'.$thumbnail));
            }
        }

        Courses::where('id_courses',$id)->delete();
        return redirect('/course/course');
    }
}
