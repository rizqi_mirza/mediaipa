<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CategoryCourses;
use Illuminate\Http\Request;
use Session;



class CategoryCourseController extends Controller
{
    public function post(Request $request){
        $this->validate($request,[
            "title_category_course" => 'required',
            "description_category_course" => 'required',
            "image_category_course" => 'image|mimes:jpeg,png,jpg|max:2048|nullable'
        ]);

        $image = $request->file('image_category_course');
        if($image != ''){
            $name_image = time().'_'.$image->getClientOriginalName();
            $destination = 'files/categorycourse';
            $image->move($destination,$name_image);
        }else{
            $name_image = "null";
        }


        $categoryCourse = new CategoryCourses;
        $categoryCourse->name_category_courses = $request->title_category_course;
        $categoryCourse->description_category_courses = $request->description_category_course;
        $categoryCourse->banner_image_category_courses = $name_image;
        $payload = $categoryCourse->save();

        if($payload){
            $request->session()->flash('status','Sub Materi Berhasil Ditambahkan');
            return redirect('/category-course');
        }
    }

    public function edit($title){
        $name= Session::get('name');
        $categoryCourse = CategoryCourses::where('name_category_courses',$title)->first();
        return view('admin.category-course.edit-category-course', compact('categoryCourse','name'));
    }

    public function update(Request $request,$id){
        // dd($request);
        $imageOld = $request->image_category_course_old;
        $imageNew = $request->file('image_category_course');

        if($imageNew != ""){
            $this->validate($request,[
                "title_category_course" => 'required',
                "description_category_course" => 'required',
                "image_category_course" => 'image|mimes:jpeg,png,jpg|max:2048|required'
            ]);
            $imageNew = $request->file('image_category_course');
            $imageName =  time()."_".$imageNew->getClientOriginalName();
            $destination = 'files/categorycourse';
            $imageNew->move($destination,$imageName);
            if(file_exists(public_path('files/categorycourse/'.$imageOld))){
                unlink(public_path('files/categorycourse/'.$imageOld));
            }
        }else{
            $imageName = $imageOld;
            $request->validate([
                "title_category_course" => 'required',
                "description_category_course" => 'required',
            ]);
        }

        $formdata =  array(
            'name_category_courses' => $request->title_category_course,
            'description_category_courses' => $request->description_category_course,
            'banner_image_category_courses' => $imageName
        );
        // dd($formdata);
        CategoryCourses::where('id_category_courses',$id)->update($formdata);
        return redirect('/category-course')->with('success','Data berhasil di update');

    }

    public function delete($id){
        $categoryCourse = CategoryCourses::findOrFail($id);
        $path = $categoryCourse->banner_image_category_courses;
        if($path != "" || $path != 'null'){
            if(file_exists(public_path('files/categorycourse/'.$path))){
                unlink(public_path('files/categorycourse/'.$path));
            }
        }
        // dd($categoryCourse)->toArray();
        CategoryCourses::where('id_category_courses',$id)->delete();
        return redirect('/category-course');
    }
}
