<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User as AppUser;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Session;


class AuthController extends Controller
{
    //
    public function postLogin(Request $request){
        $email  = $request->email;
        $password = $request->password;

        $user = User::where('email',$email)
                    ->where('password',md5($request->password))->first();
        if($user){
            if(md5($password,$user->password)){
                Session::put('name',$user->name);
                Session::put('email',$user->email);
                Session::put('login',TRUE);
                return redirect('/dashboard');
            }
            else{
                return redirect('/login-admin');
            }
        }else{
            return redirect('/login-admin');
        }
    }

    public function logout(){
        Session::flush();
        return redirect('/login-admin');
    }
}
