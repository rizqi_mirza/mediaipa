<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\QuizFormatif;
use App\Models\QuestionQuizFormatif;
use App\Models\AnswerFormatif;
use Session;


class QuestionFormatifController extends Controller
{
    //
    public function post(Request $request,$title){
        $saveId = QuizFormatif::where('name_quiz_formatif',$title)->first();

        $this->validate($request,[
            "question_quiz" => "required",
            "answer_quiz" => "required",
            "image_question" => 'image|mimes:jpeg,png,jpg|max:2048|nullable',
            "voice_question" => 'nullable|mimes:audio/mpeg,mpga,mp3,wav,aac',
            "video_question" => 'mimes:mp4|nullable'
        ]);

        $image = $request->file('image_question');
        if($image != null){
            $name_image = time().'_'.$image->getClientOriginalName();
            $destination = 'files/quiz-formatif/question';
            $image->move($destination,$name_image);
        }else{
            $name_image = "null";
        }

        // $video = $request->file('video_question');
        // if($video != null){
        //     $name_video = time().'_'.$video->getClientOriginalName();
        //     $destinationvideo = 'files/quiz-formatif/question/video';
        //     $video->move($destinationvideo,$name_video);
        // }else{
        //     $name_video = "null";
        // }

        $voice = $request->file('voice_question');
        if($voice != null){
            $name_voice = time().'_'.$voice->getClientOriginalName();
            $destinationvoice = 'files/quiz-formatif/question/voice';
            $voice->move($destinationvoice,$name_voice);
        }else{
            $name_voice = "null";
        }


        $question = new QuestionQuizFormatif();
        $question->id_quizFormatif = $saveId->id_quiz_formatif;
        $question->question_quiz_formatif = $request->question_quiz;
        $question->image_question_formatif = $name_image;
        // $question->video_question_formatif = $name_video;
        $question->voice_question_formatif = $name_voice;
        $question->correct_answer = $request->answer_quiz;
        $payload = $question->save();

        // dd($payload);
        return redirect('/questionFormatif/'.$title);
    }

    public function edit($id,$title){
        $name= Session::get('name');
        $question = QuestionQuizFormatif::where('id_question_quiz_formatif',$id)->first();
        // dd($question);
        return view('admin.quiz-formatif.questionQuiz.edit_question',compact('title','question','name'));
    }

    public function update(Request $request,$title){
        $this->validate($request,[
            "question_quiz" => "required",
            "answer_quiz" => "required",
            "image_question" => 'image|mimes:jpeg,png,jpg|max:2048|nullable',
            "voice_question" => 'nullable|mimes:audio/mpeg,mpga,mp3,wav,aac'
            // "video_question" => 'mimes:mp4|nullable'
        ]);
        $id = $request->id_question_formatif;
        $saveId = QuizFormatif::where('name_quiz_formatif',$title)->first();

        $imageOld = $request->imageOld;
        $image = $request->file('image_question');
        if($image != ''){
            $image = $request->file('image_question');
            $name_image = time().'_'.$image->getClientOriginalName();
            $destination = 'files/quiz-formatif/question';
            $image->move($destination,$name_image);
            if(file_exists(public_path('files/quiz-formatif/question/'.$imageOld))){
                unlink(public_path('files/quiz-formatif/question/'.$imageOld));
            }
        }else{
            $name_image = $imageOld;
        }

        // $videoOld = $request->videoOld;
        // $video = $request->file('video_question');
        // if($video != ''){
        //     $name_video = time().'_'.$video->getClientOriginalName();
        //     $destinationvideo = 'files/quiz-formatif/question/video';
        //     $video->move($destinationvideo,$name_video);
        //     if(file_exists(public_path('files/quiz-formatif/question/video'.$videoOld))){
        //         unlink(public_path('files/quiz-formatif/question/video'.$videoOld));
        //     }
        // }else{
        //     $name_video = $videoOld;
        // }

        $voiceOld = $request->voiceOld;
        $voice = $request->file('voice_question');
        if($voice != ''){
            $name_voice = time().'_'.$voice->getClientOriginalName();
            $destinationvoice = 'files/quiz-formatif/question/voice';
            $voice->move($destinationvoice,$name_voice);
            if(file_exists(public_path('files/quiz-formatif/question/voice/'.$voiceOld))){
                unlink(public_path('files/quiz-formatif/question/voice/'.$voiceOld));
            }
        }else{
            $name_voice = $voiceOld;
        }
        $data = array(
            "id_question_quiz_formatif" => $id,
            "id_quizFormatif" => $saveId->id_quiz_formatif,
            "question_quiz_formatif" => $request->question_quiz,
            "image_question_formatif" => $name_image,
            // "video_question_formatif" => $name_video,
            "voice_question_formatif" => $name_voice,
            "correct_answer" => $request->answer_quiz
        );

        QuestionQuizFormatif::where('id_question_quiz_formatif',$id)->update($data);
        return redirect('/questionFormatif/'.$title);

    }

    public function delete($title,$id){
        $question =  QuestionQuizFormatif::findOrFail($id);
        // dd($id);
        $path = $question->image_question_formatif;
        if($path !='' || $path !='null'){
            if(file_exists(public_path('files/quiz-formatif/question/'.$path))){
                unlink(public_path('files/quiz-formatif/question/'.$path));
            }
        }

        $audio = $question->voice_question_formatif;
        if($audio !='' || $audio !='null'){
            if(file_exists(public_path('files/quiz-formatif/question/voice/'.$audio))){
                unlink(public_path('files/quiz-formatif/question/voice/'.$audio));
            }
        }

        QuestionQuizFormatif::where('id_question_quiz_formatif',$id)->delete();
        return redirect('/questionFormatif/'.$title);
    }

    public function detail($id,$title){
        $name= Session::get('name');
        $question = QuestionQuizFormatif::where('id_question_quiz_formatif',$id)->first();

        return view('admin.quiz-formatif.questionQuiz.detail_question', compact('name','question','title'));
    }
}
