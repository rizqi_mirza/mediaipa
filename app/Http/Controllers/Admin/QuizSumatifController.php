<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\QuizSumatif;
use App\Models\CategoryCourses;
use App\Models\QuestionQuizSumatif;
use Session;


class QuizSumatifController extends Controller
{
    //
    public function post(Request $request){
        // dd($request);
        $this->validate($request,[
            "name_quiz" => "required",
            "category_id" => "required",
            "description_quiz" => 'required',
            "image_quizSumatif" => 'image|mimes:jpeg,png,jpg|max:2048|nullable'
        ]);

        $image = $request->file('image_quizSumatif');
        if($image != null){
            $name_image = time().'_'.$image->getClientOriginalName();
            $destination = 'files/quiz-sumatif';
            $image->move($destination,$name_image);
        }else{
            $name_image = "null";
        }


        $sumatif = new QuizSumatif;
        $sumatif->id_categoryCourse = $request->category_id;
        $sumatif->name_quiz_sumatif = $request->name_quiz;
        $sumatif->description_quiz_sumatif = $request->description_quiz;
        $sumatif->image_quiz_sumatif = $name_image;
        $payload = $sumatif->save();
        // dd($payload);

        return redirect('/quizSumatif');

    }

    public function edit($title){
        $name= Session::get('name');
        $quiz = QuizSumatif::where('name_quiz_sumatif',$title)->first();
        $items = CategoryCourses::all(['id_category_courses','name_category_courses']);
        return view('admin.quiz-sumatif.quizTitle.edit_quiz', compact('quiz','items','name'));
    }

    public function update(Request $request,$id){
        $imageOld = $request->imageOld;
        $image = $request->file('image_quizSumatif');

        if($image !=''){
            $this->validate($request,[
                "name_quiz" => "required",
                "category_id" => "required",
                "description_quiz" => 'required',
                "image_quizSumatif" => 'image|mimes:jpeg,png,jpg|max:2048|nullable'
            ]);
            $image = $request->file('image_quizSumatif');
            $name_image = time().'_'.$image->getClientOriginalName();
            $destination = 'files/quiz-sumatif';
            $image->move($destination,$name_image);
            if(file_exists(public_path('files/quiz-sumatif/'.$imageOld))){
                unlink(public_path('files/quiz-sumatif/'.$imageOld));
            }
        }else{
            $name_image =$imageOld;
            $request->validate([
                "name_quiz" => "required",
                "category_id" => "required",
                "description_quiz" => 'required'
            ]);
        }

        $data = array(
            'id_categoryCourse' => $request->category_id,
            'name_quiz_sumatif' => $request->name_quiz,
            'description_quiz_sumatif' => $request->description_quiz,
            'image_quiz_sumatif' => $name_image
        );

        QuizSumatif::where('id_quiz_sumatif',$id)->update($data);
        return redirect('/quizSumatif');
    }

    public function delete($id){
        if($id !=""){
            $deleteQuestion = QuestionQuizSumatif::findOrFail($id);
            $path = $deleteQuestion->image_question_sumatif;
            if($path != "" || $path != 'null'){
                if(file_exists(public_path('files/quiz-sumatif/question/'.$path))){
                    unlink(public_path('files/quiz-sumatif/question/'.$path));
                }
            }
            QuestionQuizSumatif::where('id_quizSumatif',$id)->delete();
            $deleteQuiz = QuizSumatif::findOrFail($id);
            $path2 = $deleteQuiz->image_quiz_sumatif;
            if($path2 != "" || $path2 != 'null'){
                if(file_exists(public_path('files/quiz-sumatif/'.$path2))){
                    unlink(public_path('files/quiz-sumatif/'.$path2));
                }
            }
            QuizSumatif::where('id_quiz_sumatif',$id)->delete();
        }else{
            $deleteQuiz = QuizSumatif::findOrFail($id);
            $path3 = $deleteQuiz->image_quiz_sumatif;
            if($path3 != "" || $path3 != 'null'){
                if(file_exists(public_path('files/quiz-sumatif/'.$path3))){
                    unlink(public_path('files/quiz-sumatif/'.$path3));
                }
            }
            QuizSumatif::where('id_quiz_sumatif',$id)->delete();

        }

        return redirect('/quizSumatif');


    }

}
