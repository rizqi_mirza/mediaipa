<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ResultFormatif;
use App\Models\ResultSumatif;
use App\Models\QuizFormatif;
use App\Models\QuizSumatif;

class ResultController extends Controller
{
    public function resultFormatif(){
        $resultFormatif = ResultFormatif::join();

        return view('admin.result.result_formatif');
    }
}
