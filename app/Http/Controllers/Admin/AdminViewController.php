<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CategoryCourses;
use App\Models\Courses;
use App\Models\QuestionQuizFormatif;
use App\Models\QuizSumatif;
use App\Models\QuestionQuizSumatif;
use App\Models\QuizFormatif;
use App\Models\ResultFormatif;
use App\Models\ResultSumatif;
use Session;

class AdminViewController extends Controller
{
    public function dashboard(){
        $name= Session::get('name');
        return view('admin.dashboard',compact('name'));
    }

    public function categoryCourse(){
        $name= Session::get('name');
        $categoryCourse = CategoryCourses::all();
        return view('admin.category-course.index',compact('categoryCourse','name'));
    }

    public function addCategoryCourse(){
        $name= Session::get('name');
        return view('admin.category-course.add-category-course',compact('name'));
    }

    public function course(){
        $name= Session::get('name');
        $course = Courses::join('category_courses','courses.id_categoryCourses','=','category_courses.id_category_courses')->simplePaginate(8);
        return view('admin.course.index',compact('course','name'));
    }

    public function addCourse(){
        $name= Session::get('name');
        $items = CategoryCourses::all('id_category_courses','name_category_courses');
        return view('admin.course.add-course', compact('items','name'));
    }

    public function quiz(){
        $name= Session::get('name');
        return view('admin.quiz',compact('name'));
    }

    public function quizSumatif(){
        $name= Session::get('name');
        $quizSumatif = QuizSumatif::join('category_courses','quiz_sumatif.id_categoryCourse','=','category_courses.id_category_courses')->get();
        return view('admin.quiz-sumatif.quiz_sumatif', compact('quizSumatif','name'));
    }

    public function addQuizSumatif(){
        $name= Session::get('name');
        $items = CategoryCourses::all('id_category_courses','name_category_courses');
        return view('admin.quiz-sumatif.quizTitle.add_quiz',compact('items','name'));
    }
    public function questionSumatif($title){
        $name= Session::get('name');
        $quiz =  QuestionQuizSumatif::where('name_quiz_sumatif',$title)->join('quiz_sumatif',
        'question_quiz_sumatif.id_quizSumatif','quiz_sumatif.id_quiz_sumatif')->get();
        return view('admin.quiz-sumatif.question_quizSumatif', compact('quiz','title','name'));
    }

    public function createQuestionSumatif($title){
        $name= Session::get('name');
        return view('admin.quiz-sumatif.questionQuiz.add_question', compact('title','name'));
    }

    public function addOptionSumatif($id){
        $name= Session::get('name');
        $question = QuestionQuizSumatif::where('id_question_quiz_sumatif',$id)->first();
        // dd($question);
        return view('admin.quiz-sumatif.optionQuiz.add_options', compact('question','name'));
    }

    public function quizFormatif(){
        $name= Session::get('name');
        $quizFormatif = QuizFormatif::join('courses','quiz_formatif.courseID','=','courses.id_courses')->get();
        return view('admin.quiz-formatif.quiz_formatif',compact('quizFormatif','name'));
    }

    public function addQuizFormatif(){
        $name= Session::get('name');
        $items = Courses::all('id_courses','title_courses');
        return view('admin.quiz-formatif.quizTitle.add_quiz',compact('items','name'));
    }

    public function questionFormatif($title){
        $name= Session::get('name');
        $quiz = QuestionQuizFormatif::where('name_quiz_formatif',$title)->join('quiz_formatif',
        'question_quiz_formatif.id_quizFormatif','quiz_formatif.id_quiz_formatif')->get();
        return view('admin.quiz-formatif.question_quizFormatif', compact('quiz','title','name'));
    }
    public function addQuestionFormatif($title){
        $name= Session::get('name');
        return view('admin.quiz-formatif.questionQuiz.add_question', compact('title','name'));
    }

    public function resultFormatif(){
        $name= Session::get('name');
        $resultFormatif = ResultFormatif::join('quiz_formatif','result_formatif.quizformatif_ID', '=', 'quiz_formatif.id_quiz_formatif')->get();
        return view('admin.result.result_formatif',compact('resultFormatif','name'));
    }

    public function resultSumatif(){
        $name= Session::get('name');
        $resultSumatif = ResultSumatif::join('quiz_sumatif','result_sumatif.quizsumatif_ID', '=', 'quiz_sumatif.id_quiz_sumatif')->get();
        return view('admin.result.result_sumatif',compact('resultSumatif','name'));
    }

    public function pageLogin(){
        return view('login');
    }
}
