<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\QuizFormatif;
use App\Models\CategoryCourses;
use App\Models\Courses;
use App\Models\QuestionQuizFormatif;
use Session;


class QuizFormatifController extends Controller
{
    public function post(Request $request){
        $this->validate($request,[
            "name_quiz" => "required",
            "category_id" => "required",
            "description_quiz" => 'required',
            "image_quizFormatif" => 'image|mimes:jpeg,png,jpg|max:2048|nullable'
        ]);

        $image = $request->file('image_quizFormatif');
        if($image && $image->isValid()){
            $name_image = time().'_'.$image->getClientOriginalName();
            $destination = 'files/quiz-formatif';
            $image->move($destination,$name_image);
        }else{
            $name_image = "null";
        }


        $formatif = new QuizFormatif;
        $formatif->courseID = $request->category_id;
        $formatif->name_quiz_formatif = $request->name_quiz;
        $formatif->description_quiz_formatif = $request->description_quiz;
        $formatif->image_quiz_formatif = $name_image;
        $payload = $formatif->save();
        // dd($payload);

        return redirect('/quizFormatif');
    }

    public function edit($title){
        $name= Session::get('name');
        $quiz = QuizFormatif::where('name_quiz_formatif',$title)->first();
        $items = Courses::all(['id_courses','title_courses']);
        return view('admin.quiz-formatif.quizTitle.edit_quiz', compact('quiz','items','name'));
    }

    public function update(Request $request,$id){
        $imageOld = $request->imageOld;
        $image = $request->file('image_quizSumatif');

        if($image !=''){
            $this->validate($request,[
                "name_quiz" => "required",
                "category_id" => "required",
                "description_quiz" => 'required',
                "image_quizFormatif" => 'image|mimes:jpeg,png,jpg|max:2048|nullable'
            ]);
            $image = $request->file('image_quizSumatif');
            $name_image = time().'_'.$image->getClientOriginalName();
            $destination = 'files/quiz-formatif';
            $image->move($destination,$name_image);
            if(file_exists(public_path('files/quiz-formatif/'.$imageOld))){
                unlink(public_path('files/quiz-formatif/'.$imageOld));
            }
        }else{
            $name_image =$imageOld;
            $request->validate([
                "name_quiz" => "required",
                "category_id" => "required",
                "description_quiz" => 'required'
            ]);
        }

        $data = array(
            'courseID' => $request->category_id,
            'name_quiz_formatif' => $request->name_quiz,
            'description_quiz_formatif' => $request->description_quiz,
            'image_quiz_formatif' => $name_image
        );

        QuizFormatif::where('id_quiz_formatif',$id)->update($data);
        return redirect('/quizFormatif');
    }

    public function delete($id){
        if($id !=''){
            $deleteQuestion = QuestionQuizFormatif::findOrFail($id);
            $path = $deleteQuestion->image_question_formatif;
            if($path != "" || $path != 'null'){
                if(file_exists(public_path('files/quiz-formatif/question/'.$path))){
                    unlink(public_path('files/quiz-formatif/question/'.$path));
                }
            }
            QuestionQuizFormatif::where('id_quizFormatif',$id)->delete();

            $deleteQuiz = QuizFormatif::findOrFail($id);
            $path2 = $deleteQuiz->image_quiz_formatif;
            if($path2 != "" || $path2 != 'null'){
                if(file_exists(public_path('files/quiz-formatif/'.$path2))){
                    unlink(public_path('files/quiz-formatif/'.$path2));
                }
            }
            QuizFormatif::where('id_quiz_formatif',$id)->delete();
        }else{
            $deleteQuiz = QuizFormatif::findOrFail($id);
            $path3 = $deleteQuiz->image_quiz_formatif;
            if($path3 != "" || $path3 != 'null'){
                if(file_exists(public_path('files/quiz-formatif/'.$path3))){
                    unlink(public_path('files/quiz-formatif/'.$path3));
                }
            }
            QuizFormatif::where('id_quiz_formatif',$id)->delete();
        }

        return redirect('/quizFormatif');
    }
}
