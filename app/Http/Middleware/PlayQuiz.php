<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class PlayQuiz
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Session::get('nisSiswa')){
            return redirect('/login-quiz');
        }
        return $next($request);
    }
}
