<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class AuthLoginAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Session::get('name')){
            return redirect('/login-admin');
        }
        return $next($request);
    }
}
