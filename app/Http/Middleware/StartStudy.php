<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class StartStudy
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Session::get('nisSiswa')){
            return redirect('/start-study');
        }
        return $next($request);
    }
}
