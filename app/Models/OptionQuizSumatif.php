<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OptionQuizSumatif extends Model
{
    //
    protected $table = "option_question_sumatif";
    protected $primaryKey = "id_option_sumatif";
    protected $fillable = "option_sumatif";
}
