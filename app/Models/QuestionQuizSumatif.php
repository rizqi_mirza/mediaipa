<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionQuizSumatif extends Model
{
    //
    protected $table = "question_quiz_sumatif";
    protected $primaryKey = "id_question_quiz_sumatif";
    protected $fillable = ["question_quiz_sumatif","image_question_sumatif","video_question_sumatif","voice_question_sumatif","answer1", "answer2", "answer3","correct_answer"];

    // public function quizSumatif()
    // {
    //     return $this->hasMany('App\Models\QuizSumatif');
    // }
}

