<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuizFormatif extends Model
{
    //
    protected $table = 'quiz_formatif';
    protected $primaryKey = 'id_quiz_formatif';
    protected $fillable = ['name_quiz_formatif','description_quiz_formatif','image_quiz_formatif'];
}
