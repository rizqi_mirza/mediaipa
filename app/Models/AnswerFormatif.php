<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnswerFormatif extends Model
{
    //
    protected $table = "answer_formatif";
    protected $primaryKey = "id_answer_formatif";
    protected $fillable = ["answer_formatif"];
}
