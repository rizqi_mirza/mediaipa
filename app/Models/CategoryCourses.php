<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryCourses extends Model
{
    protected $table = 'category_courses';
    protected $primaryKey = 'id_category_courses';
    protected $fillable = ['name_category_courses','description_category_courses','banner_image_category_courses'];
}
