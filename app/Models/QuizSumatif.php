<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\QuestionQuizSumatif;
class QuizSumatif extends Model
{
    //
    protected $table = 'quiz_sumatif';
    protected $primaryKey = 'id_quiz_sumatif';
    protected $fillable = ['name_quiz_sumatif','description_quiz_sumatif','image_quiz_sumatif'];

//     public function questions()
//     {
//         return $this->hasMany('App\Models\QuestionQuizSumatif');
//     }
}

