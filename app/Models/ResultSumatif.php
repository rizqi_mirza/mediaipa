<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResultSumatif extends Model
{
    protected $table = "result_sumatif";
    protected $primaryKey = "id_result_sumatif";
    protected $fillable = ['name_students','point_sumatif','correct_sumatif','wrong_sumatif'];
}
