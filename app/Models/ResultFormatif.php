<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResultFormatif extends Model
{
    protected $table = "result_formatif";
    protected $primaryKey = "id_result_formatif";
    protected $fillable = ['name_students','point_formatif','correct_formatif','wrong_formatif'];
}
