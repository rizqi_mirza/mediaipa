<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionQuizFormatif extends Model
{
    //
    protected $table = "question_quiz_formatif";
    protected $primaryKey = "id_question_quiz_formatif";
    protected $fillable = ["question_quiz_formatif","image_question_formatif","video_question_formatif","voice_question_formatif","correct_answer"];
}
