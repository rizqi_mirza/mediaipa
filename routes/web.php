<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'User\FrontendViewController@index');
Route::get('/instruction','User\FrontendViewController@instruction')->name('instruction');
Route::get('/about', 'User\FrontendViewController@about')->name('about');
Route::get('/competence', 'User\FrontendViewController@competence')->name('competence');
Route::get('/start-study','User\FrontendViewController@startStudy')->name('startStudy');
Route::post('/play-study','User\StartStudyController@loginStudy');
Route::group(['middleware' =>['startStudy']], function(){
    Route::get('/study-play','User\StartStudyController@pageStudy');
    Route::get('/sub-course','User\FrontendViewController@subCourse')->name('subCourses');
    Route::get('/courses/{title}','User\FrontendViewController@course')->name('courses');
    Route::get('/detail-course/{title}','User\FrontendViewController@detailCourse');
    Route::get('/quiz/play-formatif/{id}','User\playQuizFormatifController@detailQuizFormatif')->name('playingFormatif');
    // Route::post('quiz/play-formatif/{id}/result','User\playQuizFormatifController@resultFormatif')->name('resultFormatif');
    Route::get('/playQuiz/{id}', 'User\playQuizFormatifController@playQuiz');
    Route::get('/check-answer/{id}','User\playQuizFormatifController@checkAnswer');
    Route::get('/result/{id}','User\playQuizFormatifController@result');
    //latihan soal
    Route::get('/quiz/sumatif', 'User\PlayQuizSumatifController@quizSumatif')->name('quizSumatif');
    Route::get('/quiz/play-sumatif/{id}','User\PlayQuizSumatifController@detailQuizSumatif')->name('playingSumatif');
    Route::post('/quiz/playsumatif/{id}/result','User\PlayQuizSumatifController@result')->name('quizSumatifResult');
});
Route::get('/logout','User\StartStudyController@logout')->name('logout');


Route::get('/login-admin','Admin\AdminViewController@pageLogin');
route::post('/post-login','Admin\AuthController@postLogin');

route::group(['middleware'=>['authAdmin']],function(){
    route::get('/dashboard', 'Admin\AdminViewController@dashboard')->name('dashboard');
    //category-courses
    route::get('/category-course','Admin\AdminViewController@categoryCourse')->name('category-course');
    route::get('/add/category/course', 'Admin\AdminViewController@addCategoryCourse')->name('add-category-course');
    route::post('/add/category/course', 'Admin\CategoryCourseController@post')->name('post-category-course');
    route::get('/edit/category/course/{title}', 'Admin\CategoryCourseController@edit')->name('edit-category-course');
    route::post('/update/category/course/{id}','Admin\CategoryCourseController@update');
    route::get('/delete-category-course/{id}','Admin\CategoryCourseController@delete');

    //courses
    route::get('/course/course','Admin\AdminViewController@course')->name('course');
    route::get('/add/course','Admin\AdminViewController@addCourse')->name('add-course');
    route::post('/add/course/course','Admin\CourseController@post')->name('post-course');
    route::get('/detail/course/{title}','Admin\CourseController@detailCourse');
    route::get('/edit/course/{title}','Admin\CourseController@edit');
    route::post('/update/course/{title}','Admin\CourseController@update');
    route::get('/delete/course/{id}','Admin\CourseController@delete');

    //quiz
    route::get('/quiz','Admin\AdminViewController@quiz')->name('quiz');


    //quiz-sumatif-routing
    route::get('/quizSumatif','Admin\AdminViewController@quizSumatif');
    route::get('/add-QuizSumatif','Admin\AdminViewController@addQuizSumatif')->name('add-quizSumatif');
    route::post('/post-QuizSumatif','Admin\QuizSumatifController@post')->name('post-quizSumatif');
    route::get('/edit-quizSumatif/{title}','Admin\QuizSumatifController@edit');
    route::post('/update-QuizSumatif/{id}', 'Admin\QuizSumatifController@update');
    route::get('/delete/quizSumatif/{id}', 'Admin\QuizSumatifController@delete');

    route::get('/questionSumatif/{title}','Admin\AdminViewController@questionSumatif');
    route::get('/create-QuestionSumatif/{title}','Admin\AdminViewController@createQuestionSumatif');
    route::post('/post-QuestionSumatif/{title}', 'Admin\QuestionSumatifController@post');
    route::get('/add-optionQuestion/{id}','Admin\AdminViewController@addOptionSumatif');
    route::post('/post-optionQuestion/{question}','Admin\OptionSumatifController@post');
    route::get('/edit-question-sumatif/{titleQuestion}/{title}','Admin\QuestionSumatifController@edit');
    route::post('/update-QuestionSumatif/{title}', 'Admin\QuestionSumatifController@update');
    route::get('/delete-question-sumatif/{id}/{title}', 'Admin\QuestionSumatifController@delete');
    route::get('/detail-question-sumatif/{id}/{title}','Admin\QuestionSumatifController@detail');

    //quiz-formatif
    route::get('/quizFormatif','Admin\AdminViewController@quizFormatif');
    route::get('/add-QuizFormatif','Admin\AdminViewController@addQuizFormatif')->name('add-quizFormatif');
    route::post('/post-QuizFormatif','Admin\QuizFormatifController@post');
    route::get('/edit-quiz-formatif/{title}','Admin\QuizFormatifController@edit');
    route::post('/update-QuizFormatif/{id}','Admin\QuizFormatifController@update');
    route::get('/delete-quiz-formatif/{id}','Admin\QuizFormatifController@delete');


    route::get('/questionFormatif/{title}','Admin\AdminViewController@questionFormatif');
    route::get('/create-QuestionFormatif/{title}','Admin\AdminViewController@addQuestionFormatif');
    route::post('/post-QuestionFormatif/{title}','Admin\QuestionFormatifController@post');
    route::get('/edit-question-formatif/{id}/{title}','Admin\QuestionFormatifController@edit');
    route::post('/update-QuestionFormatif/{title}','Admin\QuestionFormatifController@update');
    route::get('/delete-question-formatif/{title}/{id}','Admin\QuestionFormatifController@delete');
    route::get('/detail-question-formatif/{id}/{title}','Admin\QuestionFormatifController@detail');

    //result
    route::get('/result-formatif','Admin\AdminViewController@resultFormatif');
    route::get('/result-sumatif','Admin\AdminViewController@resultSumatif');
});

route::get('/logout-admin','Admin\AuthController@logout');


