@extends('admin/master')

@section('container')
	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">
				<div class="page-header">
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="title">
								<h4>Kuis</h4>
							</div>
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">Kuis</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>

				{{-- <h4 class="h4 text-blue mb-36">Background and color</h4> --}}
				<div class="row clearfix">
					<div class="col-md-6 col-sm-12 mb-30">
						<div class="card text-black bg-warning card-box text-center">
							<div class="card-header ">Kuis Harian</div>
							<div class="card-body">
								{{-- <h5 class="card-title">Warning card title</h5> --}}
								<p class="card-text">Pengelolaan quiz formatif</p>
                                <a href="{{url('/quizFormatif')}}" class="btn btn-info">Go somewhere</a>
                            </div>
						</div>
					</div>
					<div class="col-md-6 col-sm-12 mb-30">
						<div class="card text-white bg-info card-box text-center">
							<div class="card-header ">Latihan Soal</div>
							<div class="card-body">
								{{-- <h5 class="card-title text-white text-left">Info card title</h5> --}}
								<p class="card-text">Pengelolaan quiz sumatif</p>
                                <a href="{{url('/quizSumatif')}}" class="btn btn-warning">Go somewhere</a>
                            </div>
						</div>
					</div>
				</div>
		</div>
	</div>
	<!-- js -->
	<script src="{{url('/assets-backend/vendors/scripts/core.js')}}"></script>
	<script src="{{url('/assets-backend/vendors/scripts/script.min.js')}}"></script>
	<script src="{{url('/assets-backend/vendors/scripts/process.js')}}"></script>
	<script src="{{url('/assets-backend/vendors/scripts/layout-settings.js')}}"></script>
@stop
