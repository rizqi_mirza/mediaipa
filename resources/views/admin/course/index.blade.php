@extends('admin/master')

@section('container')

<div class="main-container">
    <div class="pd-ltr-20 xs-pd-20-10">
        <div class="min-height-200px">
            <div class="page-header">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="title">
                            <h4>Materi</h4>
                        </div>
                        <nav aria-label="breadcrumb" role="navigation">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Materi</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right">
                        <div class="dropdown">
                            <a class="btn btn-primary " href="{{route('add-course')}}" role="button">
                                Tambah Materi
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                @foreach($course as $value)
                <div class="col-lg-3 col-md-6 col-sm-12 mb-30">
                    <div class="card card-box">
                        <div class="product-box">
                            <div class="producct-img">
                                <img src="{{url('/files/course/thumbnail/'.$value->thumbnail_courses)}}" alt="">
                                {{-- <video width="350" class="center" controls crossorigin>
                                    <source src="{{url('/files/course/thumbnail/'.$value->thumbnail_courses)}}" type="video/mp4" alt="">
                                </video> --}}
                            </div>
                        </div>

                        <div class="card-body">
                            <h5 class="card-title weight-500">{{$value->title_courses}}</h5>
                            {{-- <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> --}}
                            <div class="text-right">
                                <a href="{{url('/detail/course/'.$value->title_courses)}}" class="btn btn-primary"><span ><i class="icon-copy fa fa-window-maximize" aria-hidden="true"></i></span></a>
                                {{-- <a href=""  class="btn btn-success"><i class="icon-copy ion-information-circled"></i></a> --}}
                                <a href="{{url('/delete/course/'.$value->id_courses)}}" class="btn btn-danger"><i class="icon-copy ion-trash-a"></i></a>
                            </div>
                        </div>

                        <div class=''>
                            {{-- <span class='pages'>Page (1 of 2)</span><a class="prevpostslink pagenavi_no_click" href="#" style="cursor:default;">Prev</a><span class="current">1</span><a class="page" href="#">2</a><a class="nextpostslink" href="#">Next</a></div> --}}

                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="blog-pagination">
                <div class="btn-toolbar justify-content-center mb-15">
                    <div class="btn-group">
                        {{$course->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <script src="{{url('/assets-backend/vendors/scripts/core.js')}}"></script>
	<script src="{{url('/assets-backend/vendors/scripts/script.min.js')}}"></script>
	<script src="{{url('/assets-backend/vendors/scripts/process.js')}}"></script>
	<script src="{{url('/assets-backend/vendors/scripts/layout-settings.js')}}"></script>
    <script src="{{url('/assets-backend/src/plugins/plyr/dist/plyr.js')}}"></script>
	<script src="https://cdn.shr.one/1.0.1/shr.js"></script>
	<script>
		plyr.setup({
			tooltips: {
				controls: !0
			},
		});
	</script>
@stop
