@extends('admin/master')

@section('container')

<div class="main-container">
    <div class="pd-ltr-20 xs-pd-20-10">
        <div class="min-height-200px">
            <div class="page-header">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="title">
                            <h4>Detail Materi</h4>
                        </div>
                        <nav aria-label="breadcrumb" role="navigation">
                            {{-- <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Blog Detail</li>
                            </ol> --}}
                        </nav>
                    </div>
                </div>
            </div>
            <div class="blog-wrap">
                <div class="container pd-0">
                    <div class="row">
                        <div class="col-md-8 col-sm-12">
                            <div class="blog-detail card-box overflow-hidden mb-30">
                                <div class="blog-img">
                                    <video width="350" class="center" controls crossorigin>
                                        <source src="{{url('/files/course/'.$course->video_courses)}}" type="video/mp4" alt="">
                                    </video>
                                </div>
                                <div class="blog-caption">
                                    <h4 class="mb-10">{{$course->title_courses}}</h4>
                                    <h5 class="mb-10">Sub Materi: {{$course->name_category_courses}}</h5>
                                    <div class="modal-footer justify-content-right">
                                        <a href="{{url('/course/course/')}}" class="btn btn-outline-danger" data-dismiss="modal">Kembali</a>
                                        <a href="{{url('/edit/course/'.$course->title_courses)}}" class="btn btn-outline-primary" data-dismiss="modal">Edit</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- js -->
<script src="{{url('/assets-backend/vendors/scripts/core.js')}}"></script>
<script src="{{url('/assets-backend/vendors/scripts/script.min.js')}}"></script>
<script src="{{url('/assets-backend/vendors/scripts/process.js')}}"></script>
<script src="{{url('/assets-backend/vendors/scripts/layout-settings.js')}}"></script>
<script src="{{url('/assets-backend/src/plugins/plyr/dist/plyr.js')}}"></script>
	<script src="https://cdn.shr.one/1.0.1/shr.js"></script>
	<script>
		plyr.setup({
			tooltips: {
				controls: !0
			},
		});
	</script>
@stop
