@extends('admin/master')

@section('container')
<div class="main-container">
    <div class="pd-ltr-20 xs-pd-20-10">
        <div class="min-height-200px">
            <div class="page-header">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="title">
                            <h4>Tambah Sub Materi</h4>
                        </div>
                        <nav aria-label="breadcrumb" role="navigation">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/materi')}}">Materi</a></li>
                                <li class="breadcrumb-item active" aria-current="page">add-materi</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <!-- Default Basic Forms Start -->
            <div class="pd-20 card-box mb-30">
                <div class="clearfix">
                    <div class="pull-left">
                        <h4 class="text-blue h4">Form Tambah Sub Materi</h4>
                    </div>
                </div>
                <form method="POST" action="{{route('post-course')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-2 col-form-label">Judul Materi</label>
                        <div class="col-sm-12 col-md-10">
                            <input class="form-control" type="text" name="title_course">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-2 col-form-label">Sub Materi</label>
                        <div class="col-sm-12 col-md-10">
                            <select class="custom-select col-12" name="categorycourse">
                                <option selected="-">Pilih Sub Materi</option>
                                @foreach($items as $value)
                                    <option value="{{$value->id_category_courses}}">{{$value->name_category_courses}}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-2 col-form-label">Thumbnail Materi</label>
                            <div class="col-sm-12 col-md-10">
                                <input type="file" name="thumbnail_course" />
                            </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-2 col-form-label">File Video</label>
                            <div class="col-sm-12 col-md-10">
                                <input type="file" name="video_course" />
                            </div>
                    </div>
                    <div class="modal-footer justify-content-right">
                        <a href="/course/course" class="btn btn-outline-danger" data-dismiss="modal">Kembali</a>
                        <button type="submit" class="btn btn-primary" data-dismiss="modal">Submit</button>
                    </div>
                </form>
            </div>
    </div>
</div>

@stop
