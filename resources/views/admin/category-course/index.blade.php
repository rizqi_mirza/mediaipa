@extends('admin/master')

@section('container')

<div class="mobile-menu-overlay"></div>
	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">
				<div class="page-header">
					<div class="row">
						<div class="col-md-6 col-sm-12">
                        @if(Session::has('status'))
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            {{Session::get('status')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
							<div class="title">
								<h4>Data Sub Materi</h4>
							</div>
							<nav aria-label="breadcrumb" role="navigation">

								<ol class="breadcrumb">
									{{-- <li class="breadcrumb-item"><a href="index.html">Materi</a></li> --}}
									<li class="breadcrumb-item active" aria-current="page"> Data Sub Materi</li>
								</ol>
							</nav>-
						</div>
						<div class="col-md-6 col-sm-12 text-right">
							<div class="dropdown">
								<a class="btn btn-primary" href="{{route('add-category-course')}}" role="button">
									Tambah Sub Materi
								</a>
							</div>
						</div>
					</div>
				</div>
				<!-- Simple Datatable start -->
				<div class="card-box mb-30">
					<div class="pd-20">
						<h4 class="text-blue h4">Data Sub Materi</h4>
						{{-- <p class="mb-0">you can find more options <a class="text-primary" href="https://datatables.net/" target="_blank">Click Here</a></p> --}}
					</div>
					<div class="pb-20">
						<table class="data-table table stripe hover nowrap">
							<thead>
								<tr>
									<th class="table-plus datatable-nosort">No</th>
									<th>Nama Sub Materi</th>
									<th class="datatable-nosort">Action</th>
								</tr>
							</thead>
							<tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach ($categoryCourse as $value )
								<tr>
									<td class="table-plus">{{$no++}}</td>
									<td>{{$value->name_category_courses}}</td>
									<td>
										<div class="dropdown">
											<a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
												<i class="dw dw-more"></i>
											</a>
											<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
												{{-- <a class="dropdown-item" href="#"><i class="dw dw-eye"></i> View</a> --}}
												<a class="dropdown-item" href="{{url('/edit/category/course/'.$value->name_category_courses)}}"><i class="dw dw-edit2"></i> Edit</a>
												<a class="dropdown-item" href="{{url('/delete-category-course/'.$value->id_category_courses)}}"><i class="dw dw-delete-3"></i> Delete</a>
											</div>
										</div>
									</td>
								</tr>
                                @endforeach
							</tbody>
						</table>
					</div>
				</div>
				<!-- Simple Datatable End -->
			</div>
        </div>
            <script src="{{url('/assets-backend/vendors/scripts/core.js')}}"></script>
            <script src="{{url('/assets-backend/vendors/scripts/script.min.js')}}"></script>
            <script src="{{url('/assets-backend/vendors/scripts/process.js')}}"></script>
            <script src="{{url('/assets-backend/vendors/scripts/layout-settings.js')}}"></script>
            <script src="{{url('/assets-backend/src/plugins/datatables/js/jquery.dataTables.min.js')}}"></script>
            <script src="{{url('/assets-backend/src/plugins/datatables/js/dataTables.bootstrap4.min.js')}}"></script>
            <script src="{{url('/assets-backend/src/plugins/datatables/js/dataTables.responsive.min.js')}}"></script>
            <script src="{{url('/assets-backend/src/plugins/datatables/js/responsive.bootstrap4.min.js')}}"></script>
            <!-- buttons for Export datatable -->
            <script src="{{url('/assets-backend/src/plugins/datatables/js/dataTables.buttons.min.js')}}"></script>
            <script src="{{url('/assets-backend/src/plugins/datatables/js/buttons.bootstrap4.min.js')}}"></script>
            <script src="{{url('/assets-backend/src/plugins/datatables/js/buttons.print.min.js')}}"></script>
            <script src="{{url('/assets-backend/src/plugins/datatables/js/buttons.html5.min.js')}}"></script>
            <script src="{{url('/assets-backend/src/plugins/datatables/js/buttons.flash.min.js')}}"></script>
            <script src="{{url('/assets-backend/src/plugins/datatables/js/pdfmake.min.js')}}"></script>
            <script src="{{url('/assets-backend/src/plugins/datatables/js/vfs_fonts.js')}}"></script>
            <!-- Datatable Setting js -->
            <script src="{{url('/assets-backend/vendors/scripts/datatable-setting.js')}}"></script></body>
@stop
