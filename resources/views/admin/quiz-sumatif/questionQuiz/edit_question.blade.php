@extends('admin/master')

@section('container')
<div class="main-container">
    <div class="pd-ltr-20 xs-pd-20-10">
        <div class="min-height-200px">
            <div class="page-header">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="title">
                            <h4>Tambah Soal Kuis Somatif</h4>
                        </div>
                        {{-- <nav aria-label="breadcrumb" role="navigation">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/sub-materi')}}">Sub Materi</a></li>
                                <li class="breadcrumb-item active" aria-current="page">add-sub-materi</li>
                            </ol>
                        </nav> --}}
                    </div>
                </div>
            </div>
            <!-- Default Basic Forms Start -->
            <div class="pd-20 card-box mb-30">
                <div class="clearfix">
                    <div class="pull-left">
                        <h4 class="text-blue h4">Form Tambah Soal {{$title}}</h4>
                    </div>
                </div>
                <form method="POST" action='/update-QuestionSumatif/{{$title}}' enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-2 col-form-label">Soal Kuis</label>
                        <div class="col-sm-12 col-md-10">
                            <input type="hidden" name="id_question_sumatif" value="{{$question->id_question_quiz_sumatif}}">
                            <input class="form-control" type="text" name="question_quiz" value="{{$question->question_quiz_sumatif}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-2 col-form-label">Pilihan A</label>
                        <div class="col-sm-12 col-md-10">
                            <input class="form-control" type="text" name="answerA" value="{{$question->answer1}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-2 col-form-label">Pilihan B</label>
                        <div class="col-sm-12 col-md-10">
                            <input class="form-control" type="text" name="answerB"  value="{{$question->answer2}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-2 col-form-label">Pilihan C</label>
                        <div class="col-sm-12 col-md-10">
                            <input class="form-control" type="text" name="answerC"  value="{{$question->answer3}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-2 col-form-label">Jawaban Soal</label>
                        <div class="col-sm-12 col-md-10">
                            <select class="custom-select col-12" name="correct_answer">
                                <option  value="{{$question->correct_answer}}">Pilih jawaban</option>
                                <option value="answer1">{{$question->answer1}}</option>
                                <option value="answer2">{{$question->answer2}}</option>
                                <option value="answer3">{{$question->answer3}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-2 col-form-label">Foto</label>
                        <div class="col-sm-12 col-md-10">
                            <input type="hidden" name="imageOld" value="{{$question->image_question_sumatif}}">
                            <input type="file" name="image_question" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-2 col-form-label">Voice</label>
                        <div class="col-sm-12 col-md-10">
                            <input type="hidden" name="voiceOld" value="{{$question->voice_question_sumatif}}">
                            <input type="file" name="voice_question" />
                        </div>
                    </div>
                    {{-- <div class="form-group row">
                        <label class="col-sm-12 col-md-2 col-form-label">Video</label>
                        <div class="col-sm-12 col-md-10">
                            <input type="hidden" name="videoOld" value="{{$question->video_question_sumatif}}">
                            <input type="file" name="video_question" />
                        </div>
                    </div> --}}
                    <div class="modal-footer justify-content-right">
                        <a href="{{url('/questionSumatif/'.$title)}}" class="btn btn-outline-danger" data-dismiss="modal">Kembali</a>
                        <button type="submit" class="btn btn-primary" data-dismiss="modal">Submit</button>
                    </div>

                </form>
            </div>
    </div>
</div>
</div>
    <script src="{{url('/assets-backend/vendors/scripts/core.js')}}"></script>
	<script src="{{url('/assets-backend/vendors/scripts/script.min.js')}}"></script>
	<script src="{{url('/assets-backend/vendors/scripts/process.js')}}"></script>
	<script src="{{url('/assets-backend/vendors/scripts/layout-settings.js')}}"></script>
@stop
