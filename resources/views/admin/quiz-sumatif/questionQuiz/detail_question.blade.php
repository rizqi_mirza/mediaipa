@extends('admin/master')

@section('container')

<div class="main-container">
    <div class="pd-ltr-20 xs-pd-20-10">
        <div class="min-height-200px">
            <div class="page-header">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="title">
                            <h4>Detail Soal: {{$question->question_quiz_formatif}}</h4>
                        </div>
                        <nav aria-label="breadcrumb" role="navigation">
                            {{-- <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Blog Detail</li>
                            </ol> --}}
                        </nav>
                    </div>
                </div>
            </div>
            <div class="blog-wrap">
                <div class="container pd-0">
                    <div class="row">
                        <div class="col-md-8 col-sm-12">
                            <div class="blog-detail card-box overflow-hidden mb-30">
                                @if($question->image_question_sumatif == 'null')
                                <p></p>
                                @else
                                <div class="blog-img" >
                                    <img src="{{url('/files/quiz-sumatif/question/'.$question->image_question_sumatif)}}" width="500" alt="">
                                </div>
                                @endif
                                <br>
                                @if($question->voice_question_sumatif == 'null')
                                <p></p>
                                @else
                                <div class="blog-img col-lg-9 col-md-12 col-sm-12">
                                    <audio controls>
                                        <source src="{{url('/files/quiz-sumatif/question/voice/'.$question->voice_question_sumatif)}}" type="audio/mpeg" alt="">
                                    </audio>
                                </div>
                                @endif

                                {{-- @if($question->video_question_sumatif== 'null')
                                <p></p>
                                @else
                                <div class="blog-img">
                                    <video width="350" class="center" controls crossorigin>
                                        <source src="{{url('/files/quiz-sumatif/question/video/'.$question->video_question_sumatif)}}" type="video/mp4" alt="">
                                    </video>
                                </div>
                                @endif --}}
                                <div class="blog-caption">
                                    <h5 class="mb-10">Pilihan A: {{$question->answer1}}</h5>
                                    <h5 class="mb-10">Pilihan B: {{$question->answer2}}</h5>
                                    <h5 class="mb-10">Pilihan C: {{$question->answer3}}</h5>
                                    @if($question->correct_answer == "answer1")
                                    <h5 class="mb-10">Jawaban: {{$question->answer1}}</h5>
                                    @elseif($question->correct_answer == "answer2")
                                    <h5 class="mb-10">Jawaban: {{$question->answer2}}</h5>
                                    @else
                                    <h5 class="mb-10">Jawaban: {{$question->answer3}}</h5>
                                    @endif
                                    <div class="modal-footer justify-content-right">
                                        <a href="{{url('/questionSumatif/'.$title)}}" class="btn btn-outline-danger" data-dismiss="modal">Kembali</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- js -->
<script src="{{url('/assets-backend/vendors/scripts/core.js')}}"></script>
<script src="{{url('/assets-backend/vendors/scripts/script.min.js')}}"></script>
<script src="{{url('/assets-backend/vendors/scripts/process.js')}}"></script>
<script src="{{url('/assets-backend/vendors/scripts/layout-settings.js')}}"></script>
<script src="{{url('/assets-backend/src/plugins/plyr/dist/plyr.js')}}"></script>
	<script src="https://cdn.shr.one/1.0.1/shr.js"></script>
	<script>
		plyr.setup({
			tooltips: {
				controls: !0
			},
		});
	</script>
@stop
