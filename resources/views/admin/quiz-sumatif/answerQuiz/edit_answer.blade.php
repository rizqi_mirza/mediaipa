@extends('backend/master')

@section('container')
<div class="main-container">
    <div class="pd-ltr-20 xs-pd-20-10">
        <div class="min-height-200px">
            <div class="page-header">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="title">
                            <h4>Tambah Kuis Somatif</h4>
                        </div>
                        <nav aria-label="breadcrumb" role="navigation">
                            {{-- <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/sub-materi')}}">Sub Materi</a></li>
                                <li class="breadcrumb-item active" aria-current="page">add-sub-materi</li>
                            </ol> --}}
                        </nav>
                    </div>
                </div>
            </div>
            <!-- Default Basic Forms Start -->
            <div class="pd-20 card-box mb-30">
                <div class="clearfix">
                    <div class="pull-left">
                        <h4 class="text-blue h4">Form Tambah Kuis Somatif</h4>
                    </div>
                </div>
                <form method="POST" action="{{url('/post-kuis-somatif')}}">
                    @csrf
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-2 col-form-label">Nama Kuis</label>
                        <div class="col-sm-12 col-md-10">
                            <input class="form-control" type="text" name="name_kuis">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-2 col-form-label">Sub Materi</label>
                        <div class="col-sm-12 col-md-10">
                            <select class="custom-select col-12" name="submateri_id">
                                <option selected="-">Pilih Sub Materi</option>
                                @foreach($items as $submateri)
                                    <option value="{{$submateri->submateri_id}}">{{$submateri->submateri_name}}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-right">
                        <button type="submit" class="btn btn-primary" data-dismiss="modal">Submit</button>
                    </div>

                </form>
            </div>
    </div>
</div>
</div>
    <script src="{{url('/assets-backend/vendors/scripts/core.js')}}"></script>
	<script src="{{url('/assets-backend/vendors/scripts/script.min.js')}}"></script>
	<script src="{{url('/assets-backend/vendors/scripts/process.js')}}"></script>
	<script src="{{url('/assets-backend/vendors/scripts/layout-settings.js')}}"></script>
@stop
