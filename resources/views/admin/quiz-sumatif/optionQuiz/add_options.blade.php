@extends('admin/master')

@section('container')
<div class="main-container">
    <div class="pd-ltr-20 xs-pd-20-10">
        <div class="min-height-200px">
            <div class="page-header">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="title">
                            <h4>Tambah Pilihan Jawaban</h4>
                        </div>
                        {{-- <nav aria-label="breadcrumb" role="navigation">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/sub-materi')}}">Sub Materi</a></li>
                                <li class="breadcrumb-item active" aria-current="page">add-sub-materi</li>
                            </ol>
                        </nav> --}}
                    </div>
                </div>
            </div>
            <!-- Default Basic Forms Start -->
            <div class="pd-20 card-box mb-30">
                <div class="clearfix">
                    <div class="pull-left">
                        <h4 class="text-blue h4">Form Tambah Pilihan Jawaban Soal</h4>
                        <h6>Soal: {{$question->question_quiz_sumatif}}</h6>
                        <br>
                    </div>
                </div>
                <form method="POST" action="/post-optionQuestion/{{$question->question_quiz_sumatif}}">
                    @csrf
                    <div class="field_wrapper">
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-2 col-form-label">Pilihan Jawaban</label>
                            <div class="col-sm-12 col-md-8">
                                <input class="form-control" type="text" name="options[]" value="">
                                <input type="hidden" name="question_id" value="{{$question->id_question_quiz_sumatif}}">
                            </div>
                            <div class="col-sm-12 col-md-2">
                                <a class="btn btn-success" href="javascript:void(0);" id="add_button" title="Add field">TAMBAH</a>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-right">
                        <button type="submit" class="btn btn-primary" data-dismiss="modal">Submit</button>
                    </div>
                </form>
            </div>
    </div>
</div>
</div>
    <script src="{{url('/assets-backend/vendors/scripts/core.js')}}"></script>
	<script src="{{url('/assets-backend/vendors/scripts/script.min.js')}}"></script>
	<script src="{{url('/assets-backend/vendors/scripts/process.js')}}"></script>
	<script src="{{url('/assets-backend/vendors/scripts/layout-settings.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            var maxField = 3; //Input fields increment limitation
            var addButton = $('#add_button'); //Add button selector
            var wrapper = $('.field_wrapper'); //Input field wrapper
            var fieldHTML = '<div class="form-group row">';
            fieldHTML=fieldHTML + '<label class="col-sm-12 col-md-2 col-form-label">Pilihan Jawaban</label>';
            fieldHTML=fieldHTML + '<div class="col-sm-12 col-md-8"><input class="form-control" type="text" name="options[]"><input type="hidden" name="question_id" value="{{$question->id_question_quiz_sumatif}}"></div>';
            fieldHTML=fieldHTML + '<div class="col-sm-12 col-md-2"><a href="javascript:void(0);" class="remove_button btn btn-danger">HAPUS</a></div>';
            fieldHTML=fieldHTML + '</div>';
            var x = 1; //Initial field counter is 1

            //Once add button is clicked
            $(addButton).click(function(){
                //Check maximum number of input fields
                if(x < maxField){
                    x++; //Increment field counter
                    $(wrapper).append(fieldHTML); //Add field html
                }
            });

            //Once remove button is clicked
            $(wrapper).on('click', '.remove_button', function(e){
                e.preventDefault();
                $(this).parent('').parent('').remove(); //Remove field html
                x--; //Decrement field counter
            });
        });
    </script>
@stop
