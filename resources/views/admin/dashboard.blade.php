@extends('admin/master')

@section('container')
<div class="mobile-menu-overlay"></div>

<div class="main-container">
    <div class="pd-ltr-20">
        <div class="card-box pd-20 height-100-p mb-30">
            <div class="row align-items-center">
                <div class="col-md-4">
                    <img src="{{url('/assets-backend/vendors/images/banner-img.png')}}" alt="">
                </div>
                <div class="col-md-8">
                    <h4 class="font-20 weight-500 mb-10 text-capitalize">
                        Selamat Datang <div class="weight-600 font-30 text-blue">{{Session::get('name')}} !</div>
                    </h4>
                    <p class="font-18 max-width-600">Selamat datang ke halaman admin media pembelajaran IPA SLBN 1 Yogyakarta</p>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{url('/assets-backend/vendors/scripts/core.js')}}"></script>
<script src="{{url('/assets-backend/vendors/scripts/script.min.js')}}"></script>
<script src="{{url('/assets-backend/vendors/scripts/process.js')}}"></script>
<script src="{{url('/assets-backend/vendors/scripts/layout-settings.js')}}"></script>
<script src="{{url('/assets-backend/src/plugins/apexcharts/apexcharts.min.js')}}"></script>
<script src="{{url('/assets-backend/src/plugins/datatables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('/assets-backend/src/plugins/datatables/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{url('/assets-backend/src/plugins/datatables/js/dataTables.responsive.min.js')}}"></script>
<script src="{{url('/assets-backend/src/plugins/datatables/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{url('/assets-backend/vendors/scripts/dashboard.js')}}"></script>
@stop
