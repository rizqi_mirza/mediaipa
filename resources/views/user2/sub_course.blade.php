@extends('user2/master')

@section('container')
    <section class="hero-wrap hero-wrap-2" >
        <div class="overlay"></div>
            <div class="container">
                <div class="row  slider-text align-items-center">
                    <div class="ftco-animate">
                    <h1 class="mb-2 bread">{{$siswa}}</h1>
                    <a href="{{route('logout')}}" class="btn btn-danger">Logout</a>
                    </div>
                </div>
            </div>
    </section>

    <section class="ftco-section bg-light">
        <div class="container">
            <div class="text-center heading-section ftco-animate">
                {{-- <span class="subheading">Testimonial</span> --}}
              <h2 class="mb-4"><span>BAB</span> MATERI</h2>
              {{-- <p>Separated they live in. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p> --}}
            </div>
            <div class="row">
                @foreach ( $subCourse as $value)
                <div class="col-md-6 col-lg-4 ftco-animate">
                    <div class="blog-entry">
                        <a href="blog-single.html" class="block-20 d-flex align-items-end" style="background-image: url('/files/categorycourse/{{$value->banner_image_category_courses}}');">
                            <div class="meta-date text-center p-2"></div>
                        </a>
                        <div class="text bg-white  text-center p-4">
                            <h3 class="heading"><a href="/courses/{{$value->name_category_courses}}">{{$value->name_category_courses}}</a></h3>
                            {{-- <p>{{$value->description_category_courses}}</p> --}}
                            <p class="mb-0 "><a href="/courses/{{$value->name_category_courses}}" class="btn btn-secondary ">Belajar</a></p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="row no-gutters my-5">
                <div class="col text-center">
                    <div class="block-27">
                        <ul>
                            <a type="submit" href="/study-play" class="btn btn-primary">Kembali</a>

                            {{$subCourse->links() }}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
