@extends('user2/master')

@section('container')

<section class="ftco-section testimony-section bg-light">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-2">
            <div class="col-md-8 text-center heading-section ftco-animate">
                {{-- <span class="subheading">Testimonial</span> --}}
            <h2 class="mb-0"><span>Standart </span>Kompetensi</h2>
            {{-- <p>Separated they live in. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p> --}}
            </div>
        </div>
    </div>
</section>
<section class="ftco-section">
    <div class="container">
        <div class="row ">
            <div class="col-md-6">
                {{-- <div class="img" style="background-image: url(assets/images/course-1.jpg);"></div> --}}
                <div class="text bg-light p-4">
                    <h3 class="text-center"><a href="#">Kompetensi Inti 3 (Pengetahuan)</a></h3>
                    {{-- <p class="subheading"><span>Class time:</span> 9:00am - 10am</p> --}}
                    <p class="text-center">Memahami pengetahuan faktual dengan cara mengamati [mendengar, melihat membaca] dan menanya berdasarkan rasa ingin tahu tentang dirinya,
                        makhluk ciptaan tuhan dan kegiatannya, dan benda-benda yang dijumpainya di rumah dan di sekolah.</p>
                </div>

                <div class="text bg-light p-4">
                    <h5 class="text-center">Kompetensi Dasar</h5>
                    <ul>
                        <li><span >3.1. Mengidentifikasi ciri-ciri benda padat, cair dan gas yang ada di lingkungan sekitar</span></li>
                        <li><span >3.2. Mengenal perubahan wujud benda (membeku dan mencair)</span></li>
                        <li><span >3.3. Mengenal musim indonesia</span></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6">
                {{-- <div class="img" style="background-image: url(assets/images/course-2.jpg);"></div> --}}
                <div class="text bg-light p-4">
                    <h3 class="text-center"><a href="#">Kompetensi Inti 4 (Pengetahuan)</a></h3>
                    {{-- <p class="subheading"><span>Class time:</span> 9:00am - 10am</p> --}}
                    <p class="text-center">Menyajikan pengetahuan faktual dalam bahasa [lisan/tulis/isyarat] yang jelas dan logis, dalam karya yang estetis, dalam gerakan
                        yang mencerminkan anak sehat dan dalam tindakan yang mencerminkan perilaku anak beriman dan berakhlak mulai.</p>
                </div>
                <div class="text bg-light p-4">
                    <h5 class="text-center">Kompetensi Dasar</h5>
                    <ul>
                        <li><span>4.1. Menceritakan [lisan/tulisan/gambar] ciri-ciri benda padat, cair, dan gas.</span></li>
                        <li><span>4.2. Melakukan percobaan sederhana perubahan wujud benda (membeku dan mencair).</span></li>
                        <li><span>4.3. Menuliskan tanda-tanda musim hujan dan kemarau.</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
