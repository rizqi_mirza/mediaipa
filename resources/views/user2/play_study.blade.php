@extends('user2/master')

@section('container')
    <section class="hero-wrap hero-wrap-2" >
        <div class="overlay"></div>
            <div class="container">
                <div class="row  slider-text align-items-center">
                    <div class="ftco-animate">
                    <h1 class="mb-2 bread">{{$siswa}}</h1>
                    <p class="breadcrumbs"><span class="mr-2"><a href="{{route('logout')}}">Logout</a></span></p>
                    </div>
                </div>
            </div>
    </section>

    <section class="ftco-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-6 ftco-animate">
                    <div class="pricing-entry bg-light pb-6 text-center">
                        <div>
                            <h3 class="mb-3">Materi</h3>
                        </div>
                        <div class="img" style="background-image: url(assets/images/materi.jpg);"></div>
                        <div class="px-4">
                            <p>Belajar Materi IPA</p>
                        </div>
                        <p class="button text-center"><a href="{{route('subCourses')}}" class="btn btn-primary px-4 py-3">Mulai Belajar</a></p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 ftco-animate">
                    <div class="pricing-entry bg-light pb-6 text-center">
                        <div>
                            <h3 class="mb-3">Latihan Soal</h3>
                        </div>
                        <div class="img" style="background-image: url(assets/images/latihan_soal.jpg);"></div>
                        <div class="px-4">
                            <p>Latihan Soal IPA</p>
                        </div>
                        <p class="button text-center"><a href="{{route('quizSumatif')}}" class="btn btn-secondary px-4 py-3">Mulai</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
