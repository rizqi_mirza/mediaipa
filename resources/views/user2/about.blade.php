@extends('user2/master')

@section('container')

<section class="ftco-section testimony-section bg-light">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-2">
            <div class="col-md-8 text-center heading-section ftco-animate">
                {{-- <span class="subheading">Testimonial</span> --}}
            <h2 class="mb-0"><span>Tentang </span>Kami</h2>
            {{-- <p>Separated they live in. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p> --}}
            </div>
        </div>
    </div>
</section>

<section class="ftco-section contact-section">
    <div class="container">
      <div class="row d-flex mb-5 contact-info">
        <div class="col-md-3 d-flex">
            <div class="bg-light align-self-stretch box p-4 text-center">
                <h3 class="mb-4">Alamat</h3>
              <p>Jl. Kapten Laut Samadikun No.3, Wirogunan, Kec. Mergangsan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55151</p>
            </div>
        </div>
        <div class="col-md-3 d-flex">
            <div class="bg-light align-self-stretch box p-4 text-center">
                <h3 class="mb-4">Nomor Telepon</h3>
              <p>(0274) 375539</p>
            </div>
        </div>
        <div class="col-md-3 d-flex">
            <div class="bg-light align-self-stretch box p-4 text-center">
                <h3 class="mb-4">Alamat email</h3>
              <p><a href="mailto:info@yoursite.com">info@yoursite.com</a></p>
            </div>
        </div>
        <div class="col-md-3 d-flex">
            <div class="bg-light align-self-stretch box p-4 text-center">
                <h3 class="mb-4">Website Utama</h3>
              <p><a href="http://www.slbn1yogyakarta.sch.id/">www.slbn1yogyakarta.sch.id</a></p>
            </div>
        </div>
      </div>
    </div>
</section>

<section class="ftco-section ftco-no-pt ftc-no-pb">
    <div class="container">
        <div class="row">
            <div class="col-md-5 order-md-last wrap-about py-5 wrap-about bg-light">
                <div class="text px-4 ftco-animate">
              <h2 class="mb-4">Tentang Media Ipa</h2>
              <p>Media IPA merupakan media pembelajaran yang dirancang khusus untuk siswa tunagrahita SDLB N 1 Yogyakarta mata pelajaran Ilmu Pengetahuan Alam supaya menjadi lebih mudah dalam melakukan kelas online dalam mengembangkan pemahaman siswa menjadi lebih baik.</p>
                    {{-- <p>Media Ipa merupakan media pembelajaran Ilmu Pengetahuan Alam yang dikhususkan sesuai kurikulum yang diberikan pada SLB N 1 Yogyakarta. Media Ipa ini digunakan untuk proses belajar secara online siswa kelas 6 SDLB N 1 Yogyakarta.</p> --}}
                    {{-- <p><a href="#" class="btn btn-secondary px-4 py-3">Tentang Kami</a></p> --}}
                </div>
            </div>
            <div class="col-md-7 wrap-about py-5 pr-md-4 ftco-animate">
              <h2 class="mb-4">Fitur Media Ipa</h2>
                <p>Fitur yang disediakan media ipa diantaranya sebagai berikut:</p>
                <div class="row mt-5">
                    <div class="col-lg-6">
                        <div class="services-2 d-flex">
                            <div class="icon mt-2 mr-3 d-flex justify-content-center align-items-center"><span class="flaticon-diploma"></span></div>
                            <div class="text">
                                <h3>Kompetensi</h3>
                                <p>Penyampaian materi berdasarkan standart kompetensi siswa SDLB N 1 Yogyakarta</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="services-2 d-flex">
                            <div class="icon mt-2 mr-3 d-flex justify-content-center align-items-center"><span class="flaticon-education"></span></div>
                            <div class="text">
                                <h3>Video Materi</h3>
                                <p>Video materi digunakan untuk mempermudah siswa dalam mengulang pembahasan materi</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="services-2 d-flex">
                            <div class="icon mt-2 mr-3 d-flex justify-content-center align-items-center"><span class="flaticon-jigsaw"></span></div>
                            <div class="text">
                                <h3>Kuis</h3>
                                <p>Kegiatan untuk meningkatkan respon pemahaman mandiri siswa</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="services-2 d-flex">
                            <div class="icon mt-2 mr-3 d-flex justify-content-center align-items-center"><span class="flaticon-kids"></span></div>
                            <div class="text">
                                <h3>Latihan Soal</h3>
                                <p>Kegiatan pengulangan dalam proses belajar agar mendapatkan hasil yang lebih baik</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-gallery">
    <div class="container-wrap">
        <div class="row no-gutters">
                <div class="col-md-3 ftco-animate">
                    <a href="assets/images/course-1.jpg" class="gallery image-popup img d-flex align-items-center" style="background-image: url(assets/images/course-1.jpg);">
                        <div class="icon mb-4 d-flex align-items-center justify-content-center">
                        <span class="icon-search"></span>
                    </div>
                    </a>
                </div>
                <div class="col-md-3 ftco-animate">
                    <a href="assets/images/image_2.jpg" class="gallery image-popup img d-flex align-items-center" style="background-image: url(assets/images/image_2.jpg);">
                        <div class="icon mb-4 d-flex align-items-center justify-content-center">
                        <span class="icon-search"></span>
                    </div>
                    </a>
                </div>
                <div class="col-md-3 ftco-animate">
                    <a href="assets/images/image_3.jpg" class="gallery image-popup img d-flex align-items-center" style="background-image: url(assets/images/image_3.jpg);">
                        <div class="icon mb-4 d-flex align-items-center justify-content-center">
                        <span class="icon-search"></span>
                    </div>
                    </a>
                </div>
                <div class="col-md-3 ftco-animate">
                    <a href="assets/images/image_4.jpg" class="gallery image-popup img d-flex align-items-center" style="background-image: url(assets/images/image_4.jpg);">
                        <div class="icon mb-4 d-flex align-items-center justify-content-center">
                        <span class="icon-search"></span>
                    </div>
                    </a>
                </div>
        </div>
    </div>
</section>

@stop
