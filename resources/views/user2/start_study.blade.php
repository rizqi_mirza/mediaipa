@extends('user2/master')

@section('container')
<section class="ftco-section testimony-section bg-light">
    <div class="container">
      <div class="row justify-content-center mb-5 pb-2">
        <div class="col-md-8 text-center heading-section ftco-animate">
            {{-- <span class="subheading">Testimonial</span> --}}
          <h2 class="mb-4"><span>Mulai</span> Belajar IPA</h2>
          {{-- <p>Separated they live in. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p> --}}
        </div>
        <div class="col-md-6 p-4 p-md-5 order-md-last bg-light text-center">
            <form action="/play-study" method="POST">
                @csrf
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Nomor Induk Siswa" name="nis_siswa">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Nama Lengkap" name="nama_siswa">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary px-4 py-3 center">Submit</button>
                </div>
            </form>
        </div>
      </div>
    </div>
  </section>


@stop
