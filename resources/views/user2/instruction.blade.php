@extends('user2/master')

@section('container')

<section class="ftco-section testimony-section bg-light">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-2">
            <div class="col-md-8 text-center heading-section ftco-animate">
                {{-- <span class="subheading">Testimonial</span> --}}
            <h2 class="mb-0"><span>Petunjuk</span> Pemakaian</h2>
            <p>Halaman ini digunakan cara pakai menggunakan media ipa </p>
            </div>
        </div>
    </div>
</section>
<br>
<section class="ftco-section ftco-no-pt ftc-no-pb">
    <div class="container">
        <div class="row justify-content-center ftco-animate mb-5 pb-2">
            <div>
                <video width="1040" height="504" controls crossorigin>
                    <source src="{{url('/assets/videos/belajaripa.mp4')}}" type="video/mp4" alt="" >
                </video>
            </div>
        </div>
    </div>
</section>
@stop
