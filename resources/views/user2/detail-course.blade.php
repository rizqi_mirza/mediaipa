@extends('user2/master')

@section('container')

<section class="hero-wrap hero-wrap-2" >
    <div class="overlay"></div>
        <div class="container">
            <div class="row  slider-text align-items-center">
                <div class="ftco-animate">
                <h1 class="mb-2 bread">{{$siswa}}</h1>
                <p class="breadcrumbs"><span class="mr-2"><a href="{{route('logout')}}" class="btn btn-danger">Logout</a></span></p>
                </div>
            </div>
        </div>
</section>
<section class="ftco-section testimony-section bg-light">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-2">
            <div class="col-md-8 text-center heading-section ftco-animate">
                {{-- <span class="subheading">Testimonial</span> --}}
                <h2 class="mb-0"><span>{{$detailCourse->title_courses}}</h2>
            {{-- <p>Separated they live in. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p> --}}
            </div>
        </div>
        <div class="row justify-content-center ftco-animate">
            <div>
                <video width="1040" height="504" controls crossorigin>
                    <source src="{{url('/files/course/'.$detailCourse->video_courses)}}" type="video/mp4" alt="" >
                </video>
            </div>
            <div class="col-md-8 text-center heading-section ftco-animate">
                <br>
                <p class="mb-0">
                    <a type="submit" href="{{url('/courses/'.$detailCourse->name_category_courses)}}" class="btn btn-danger">Kembali</a>
                    <a href="/quiz/play-formatif/{{$quiz->id_quiz_formatif}}" class="btn btn-primary">Mulai Kuis</a>
                </p>
            </div>
        </div>
        <div class="row no-gutters my-5">
            <div class="col text-center">
                <div class="block-27">
                    <ul>
                        {{-- <a type="submit" href="{{url('/courses/'.$detailCourse->name_category_courses)}}" class="btn btn-danger">Kembali</a> --}}
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
