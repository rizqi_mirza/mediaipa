@extends('user2/master')

@section('container')

<section class="hero-wrap hero-wrap-2" >
    <div class="overlay"></div>
        <div class="container">
            <div class="row  slider-text align-items-center">
                <div class="ftco-animate">
                <h1 class="mb-2 bread">{{$siswa}}</h1>
                <p class="breadcrumbs"><span class="mr-2"><a href="{{route('logout')}}">Logout</a></span></p>
                </div>
            </div>
        </div>
</section>

<section class="ftco-section testimony-section bg-light">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-2">
            <div class="col-md-8 text-center heading-section ftco-animate">
                <span class="subheading">Hasil Latihan Soal</span>
                <span  class="subheading">{{$quizID->name_quiz_sumatif}}</span>
                <h2 class="mb-4"><span>Nilai : </span>{{$point}}</h2>
                <h2 class="mb-4"><span>Benar : </span>{{$correct}}</h2>
                <h2 class="mb-4"><span>Salah : </span>{{$wrong}}</h2>
                <a href="{{route('quizSumatif')}}" class="btn btn-primary" >Selesai</a>
            </div>
        </div>
    </div>
</section>
@stop
