@extends('user2/master')

@section('container')
 <section class="home-slider owl-carousel">
    <div class="slider-item" style="background-image:url(assets/images/bg_1.png);">
        <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
        <div class="col-md-8 text-center ftco-animate">
          <h1 class="mb-4">Media Belajar IPA<span>SDLB N 1 Yogyakarta</span></h1>
          {{-- <p><a href="#" class="btn btn-secondary px-4 py-3 mt-3">Read More</a></p> --}}
        </div>
      </div>
      </div>
    </div>

    <div class="slider-item" style="background-image:url(assets/images/bg_2.png);">
        <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
        <div class="col-md-8 text-center ftco-animate">
            <h1 class="mb-4">Media Belajar IPA<span>SDLB N 1 Yogyakarta</span></h1>
          {{-- <p><a href="#" class="btn btn-secondary px-4 py-3 mt-3">Read More</a></p> --}}
        </div>
      </div>
      </div>
    </div>
  </section>

  <section class="ftco-services ftco-no-pb">
          <div class="container-wrap">
              <div class="row no-gutters">
        <div class="col-md-3 d-flex services align-self-stretch pb-4 px-4 ftco-animate bg-primary">
          <div class="media block-6 d-block text-center">
            <div class="icon d-flex justify-content-center align-items-center">
                  <span class="flaticon-teacher"></span>
            </div>
            <div class="media-body p-2 mt-3">
              <h3 class="heading">Kompetensi</h3>
              <p>Kompetensi sikap spiritual, Kompetensi sikap sosial, Kompetensi pengetahuan dan kompetensi ketrampilan secara keseluruhan sesuai tematik.</p>
            </div>
          </div>
        </div>
        <div class="col-md-3 d-flex services align-self-stretch pb-4 px-4 ftco-animate bg-tertiary">
          <div class="media block-6 d-block text-center">
            <div class="icon d-flex justify-content-center align-items-center">
                  <span class="flaticon-reading"></span>
            </div>
            <div class="media-body p-2 mt-3">
              <h3 class="heading">Video Materi</h3>
              <p>Pembelajaran online dengan pengamatan materi dalam berupa video yang disampaikan berdasarkan kompetensi siswa.</p>
            </div>
          </div>
        </div>
        <div class="col-md-3 d-flex services align-self-stretch pb-4 px-4 ftco-animate bg-fifth">
          <div class="media block-6 d-block text-center">
            <div class="icon d-flex justify-content-center align-items-center">
                  <span class="flaticon-books"></span>
            </div>
            <div class="media-body p-2 mt-3">
              <h3 class="heading">Kuis</h3>
              <p>Kegiatan latihan mandiri siswa untuk meningkatkan kemampuan respon dalam menyimak video materi yang disampaikan.</p>
            </div>
          </div>
        </div>
        <div class="col-md-3 d-flex services align-self-stretch pb-4 px-4 ftco-animate bg-quarternary">
          <div class="media block-6 d-block text-center">
            <div class="icon d-flex justify-content-center align-items-center">
                  <span class="flaticon-diploma"></span>
            </div>
            <div class="media-body p-2 mt-3">
              <h3 class="heading">Latihan Soal</h3>
              <p>Kegiatan pengulangan dalam proses pembelajaran yang bertujuan untuk memotivasi siswa lebih memahami bahan pelajaran agar
                mendapatkan hasil yang lebih baik.</p>
            </div>
          </div>
        </div>
      </div>
          </div>
      </section>

      <section class="ftco-section ftco-no-pt ftc-no-pb">
          <div class="container">
              <div class="row">
                  <div class="col-md-5 order-md-last wrap-about py-5 wrap-about bg-light">
                      <div class="text px-4 ftco-animate">
                    <h2 class="mb-4">Tentang Media Ipa</h2>
                    <p>Media IPA merupakan media pembelajaran yang dirancang khusus untuk siswa tunagrahita SDLB N 1 Yogyakarta mata pelajaran Ilmu Pengetahuan Alam supaya menjadi lebih mudah dalam melakukan kelas online dalam mengembangkan pemahaman siswa menjadi lebih baik.</p>
                          {{-- <p>Media Ipa merupakan media pembelajaran Ilmu Pengetahuan Alam yang dikhususkan sesuai kurikulum yang diberikan pada SLB N 1 Yogyakarta. Media Ipa ini digunakan untuk proses belajar secara online siswa kelas 6 SDLB N 1 Yogyakarta.</p> --}}
                          <p><a href="{{route('about')}}" class="btn btn-secondary px-4 py-3">Tentang Kami</a></p>
                      </div>
                  </div>
                  <div class="col-md-7 wrap-about py-5 pr-md-4 ftco-animate">
                    <h2 class="mb-4">Fitur Media Ipa</h2>
                      <p>Fitur yang disediakan media ipa diantaranya sebagai berikut:</p>
                      <div class="row mt-5">
                          <div class="col-lg-6">
                              <div class="services-2 d-flex">
                                  <div class="icon mt-2 mr-3 d-flex justify-content-center align-items-center"><span class="flaticon-diploma"></span></div>
                                  <div class="text">
                                      <h3>Kompetensi</h3>
                                      <p>Penyampaian materi berdasarkan standart kompetensi siswa SDLB N 1 Yogyakarta</p>
                                  </div>
                              </div>
                          </div>
                          <div class="col-lg-6">
                              <div class="services-2 d-flex">
                                  <div class="icon mt-2 mr-3 d-flex justify-content-center align-items-center"><span class="flaticon-education"></span></div>
                                  <div class="text">
                                      <h3>Video Materi</h3>
                                      <p>Video materi digunakan untuk mempermudah siswa dalam mengulang pembahasan materi</p>
                                  </div>
                              </div>
                          </div>
                          <div class="col-lg-6">
                              <div class="services-2 d-flex">
                                  <div class="icon mt-2 mr-3 d-flex justify-content-center align-items-center"><span class="flaticon-jigsaw"></span></div>
                                  <div class="text">
                                      <h3>Kuis</h3>
                                      <p>Kegiatan untuk meningkatkan respon pemahaman mandiri siswa</p>
                                  </div>
                              </div>
                          </div>
                          <div class="col-lg-6">
                              <div class="services-2 d-flex">
                                  <div class="icon mt-2 mr-3 d-flex justify-content-center align-items-center"><span class="flaticon-kids"></span></div>
                                  <div class="text">
                                      <h3>Latihan Soal</h3>
                                      <p>Kegiatan pengulangan dalam proses belajar agar mendapatkan hasil yang lebih baik</p>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </section>

  @stop
