@extends('user2/master')

@section('container')
    <section class="hero-wrap hero-wrap-2" >
        <div class="overlay"></div>
            <div class="container">
                <div class="row  slider-text align-items-center">
                    <div class="ftco-animate">
                    <h1 class="mb-2 bread">{{$siswa}}</h1>
                    <p class="breadcrumbs"><span class="mr-2"><a href="{{route('logout')}}">Logout</a></span></p>
                    </div>
                </div>
            </div>
    </section>

    <section class="ftco-section testimony-section bg-light">
        <div class="container">
            <div class="row justify-content-center mb-5 pb-2">
                <div class="col-md-8 heading-section ftco-animate">
                    {{-- <span class="subheading">Testimonial</span>
                <h2 class="mb-4"><span>What Parents</span> Says About Us</h2> --}}
                {{-- <p class="subheading">Separated they live in. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p> --}}
                @foreach($quiz as $question)
                    <form action="{{route('quizSumatifResult',$question->id_quiz_sumatif)}}" method="POST">
                        @csrf

                            <strong> {{$loop->iteration}} {{$question->question_quiz_sumatif}}</strong>
                            @if($question->image_question_sumatif == 'null')
                                <p></p>
                            @else
                            <div class="form-check">
                                <img src="{{url('/files/quiz-sumatif/question/'.$question->image_question_sumatif)}}" width="200" class="img-thumbnail"
                                alt="{{$question->question}}">
                            </div>
                            @endif
                            <div id="audio-question">
                                <audio controls>
                                    <source src="{{url('/files/quiz-sumatif/question/voice/'.$question->voice_question_sumatif)}}" type="audio/mpeg" alt="">
                                </audio>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="{{$question->id_question_quiz_sumatif}}"
                                    id="quiz{{$question->answer1}}"
                                    value="answer1" required>
                                <label class="form-check-label" for="quiz{{$question->answer1}}">
                                    {{$question->answer1}}
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="{{$question->id_question_quiz_sumatif}}"
                                    id="quiz{{$question->answer2}}"
                                    value="answer2" required>
                                <label class="form-check-label" for="quiz{{$question->answer2}}">
                                    {{$question->answer2}}
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="{{$question->id_question_quiz_sumatif}}"
                                    id="quiz{{$question->answer3}}"
                                    value="answer3" required>
                                <label class="form-check-label" for="quiz{{$question->answer3}}">
                                    {{$question->answer3}}
                                </label>
                            </div>
                            @if(!$loop->last)
                                <hr>
                            @endif
                        @endforeach


                        <div class="d-grid gap-2 mt-3">
                            <button type="submit" class="btn btn-primary">Sumbit</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>

@stop
