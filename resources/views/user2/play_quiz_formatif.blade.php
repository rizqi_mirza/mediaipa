<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Media Belajar IPA | SDLB N 1 Yogyakarta</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Fredericka+the+Great" rel="stylesheet">

    <link rel="stylesheet" href="{{url('assets/css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/animate.css')}}">

    <link rel="stylesheet" href="{{url('assets/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{url('assets/css/aos.css')}}">

    <link rel="stylesheet" href="{{url('assets/css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{url('assets/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/icomoon.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/style.css')}}">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark ftco_navbar ftco-navbar-light" id="ftco-navbar">
	    <div class="container d-flex align-items-center">
	    	<a class="navbar-brand" href="/">Media IPA</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>
	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav ml-auto">
	        	<li class="nav-item active"><a href="/" class="nav-link pl-0">Beranda</a></li>
	        	<li class="nav-item"><a href="{{route('instruction')}}" class="nav-link">Petunjuk</a></li>
	        	<li class="nav-item"><a href="{{route('startStudy')}}" class="nav-link">Belajar IPA</a></li>
	        	<li class="nav-item"><a href="{{route('competence')}}" class="nav-link">Kompetensi</a></li>
	        	<li class="nav-item"><a href="{{route('about')}}" class="nav-link">Tentang Kami</a></li>
	        	<li class="nav-item"><a href="{{url('/login-admin')}}" class="nav-link">Login</a></li>
	        </ul>
	      </div>
	    </div>
	  </nav>
    <!-- END nav -->

    <section class="hero-wrap hero-wrap-2" >
        <div class="overlay"></div>
            <div class="container">
                <div class="row  slider-text align-items-center">
                    <div class="ftco-animate">
                    <h1 class="mb-2 bread">{{$siswa}}</h1>
                    <p class="breadcrumbs"><span class="mr-2"><a href="{{route('logout')}}">Logout</a></span></p>
                    </div>
                </div>
            </div>
    </section>

<!-- Modal -->
<div class="modal fade" id="result-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body" id="modal-body">

        </div>
        <div class="modal-footer">
            {{-- <p>[[check]]</p> --}}
          {{-- <button type="button" class="btn     btn-secondary" data-dismiss="modal">Close</button> --}}
          <button type="button" class="btn btn-primary close-modal" data-dismiss="modal">selanjutnya</button>
        </div>
      </div>
    </div>
  </div>
<section class="ftco-section testimony-section bg-light">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-2">
            <div class="col-md-8 text-center heading-section ftco-animate">
                {{-- <span class="subheading">Testimonial</span>
            <h2 class="mb-4"><span>What Parents</span> Says About Us</h2> --}}
            {{-- <p class="subheading">Separated they live in. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p> --}}
            <div id="questions-tab">

            </div>
            </div>
        </div>
    </div>
</section>


<footer class="ftco-footer ftco-bg-dark ftco-section">

</footer>



<!-- loader -->
<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


<script src="{{url('assets/js/jquery.min.js')}}"></script>
<script src="{{url('assets/js/jquery-migrate-3.0.1.min.js')}}"></script>
<script src="{{url('assets/js/popper.min.js')}}"></script>
<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/js/jquery.easing.1.3.js')}}"></script>
<script src="{{url('assets/js/jquery.waypoints.min.js')}}"></script>
<script src="{{url('assets/js/jquery.stellar.min.js')}}"></script>
<script src="{{url('assets/js/owl.carousel.min.js')}}"></script>
<script src="{{url('assets/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{url('assets/js/aos.js')}}"></script>
<script src="{{url('assets/js/jquery.animateNumber.min.js')}}"></script>
<script src="{{url('assets/js/scrollax.min.js')}}"></script>
{{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script> --}}
{{-- <script src="{{url('assets/js/google-map.js')}}"></script> --}}
<script src="{{url('assets/js/main.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/oj.mustache/0.7.2/oj.mustache.min.js"></script>

<template tag="question">
    <form class="question">
        <p>[[question_quiz_formatif]]</p>
        <input type="hidden" name="id" value="[[id_question_quiz_formatif]]" id="">

        <div id="image-question">
            <img src="/files/quiz-formatif/question/[[image_question_formatif]]" width="200px" alt="">
        </div>
        {{-- @if ([[voice_question_formatif]] == null)
        <p></p>
        @else --}}
        <div id="audio-question">
            <audio controls>
                <source src="/files/quiz-formatif/question/voice/[[voice_question_formatif]]" type="audio/mpeg" alt="">
            </audio>
        </div>
        {{-- @endif --}}


        <br>
        <div >
            <input type="text" class="form-control mb-2 text-center" name="answer" id="" >
        </div>
        <br>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</template>
<script>
  Mustache.tags =['[[',']]'];

  current_question_index = 0;
  questions = $.ajax({
      url:"/playQuiz/{{$id}}",
      success: function(data){
          // console.log(data);
          return data;
      },
      async: false
  }).responseJSON;

  function render_question(data) {
      $("#questions-tab").html("")
      template = $('template[tag="question"]').html();
      component = Mustache.render(template, data)

      $("#questions-tab").append(component);

  }

  function goto_next_question() {
      current_question_index = current_question_index + 1;
      render_question(questions[current_question_index]);
  }

  render_question(questions[current_question_index]);

  $('body').on('submit', 'form.question', function (event) {
      event.preventDefault()
      $.ajax({
          url: "/check-answer/{{$id}}",
          data: $(this).serialize(),
          success: function (data) {
            //   console.log(data)
            //   $("#result-modal").modal("show");
            if(data.answer == true){
                // var data = res.body;
                // $('.modal-body').text(data);
                // 'message' => "Jawaban Anda Benar";
                $("#modal-body").html("Jawaban Anda Benar");
                $("#result-modal").modal("show");
            }else if(data.answer == false){
                // 'message' => "Jawaban Anda Salah";
                $("#modal-body").html("Jawaban Anda Salah");
                $("#result-modal").modal("show");
            }

          },
          async: false
      })
  });

  $('body').on('hide.bs.modal', '#result-modal', function (event) {
      if(questions.length-1 == current_question_index) {
          window.location.replace("/result/{{$id}}");
      }

      goto_next_question();
  });

</script>
</body>
</html>

